def test_hovering_original_highlight_translation(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    original = page.wait_for_selector(
        '#alignments-tabs #alignment-content-1-eng #align-original-1 blockquote [data-id="align-1-[2]"]'
    )
    translation = page.wait_for_selector(
        '#alignments-tabs #alignment-content-1-eng #align-translation-1 blockquote [data-id="align-1-[2]"]'
    )
    assert original.get_attribute("class") is None
    assert translation.get_attribute("class") is None
    original.hover()
    assert original.get_attribute("class") == "tag tag--warning tag--normal"
    assert translation.get_attribute("class") == "tag tag--warning tag--normal"
    # Cannot find how to un-hover, so let’s hover another element…
    page.wait_for_selector(
        '#alignments-tabs #alignment-content-1-eng #align-original-1 blockquote [data-id="align-1-[3]"]'
    ).hover()
    assert original.get_attribute("class") == ""
    assert translation.get_attribute("class") == ""


def test_hovering_original_highlight_translation_from_another_tab(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.70/")
    tab = page.wait_for_selector('#alignments-tabs [href="#alignment-content-2-fra"]')
    tab.click()
    original = page.wait_for_selector(
        '#alignments-tabs #alignment-content-2-fra #align-original-2 blockquote [data-id="align-2-[2]"]'
    )
    translation = page.wait_for_selector(
        '#alignments-tabs #alignment-content-2-fra #align-translation-2 blockquote [data-id="align-2-[2]"]'
    )
    assert original.get_attribute("class") is None
    assert translation.get_attribute("class") is None
    original.hover()
    assert original.get_attribute("class") == "tag tag--warning tag--normal"
    assert translation.get_attribute("class") == "tag tag--warning tag--normal"
    # Cannot find how to un-hover, so let’s hover another element…
    page.wait_for_selector(
        '#alignments-tabs #alignment-content-2-fra #align-original-2 blockquote [data-id="align-2-[3]"]'
    ).hover()
    assert original.get_attribute("class") == ""
    assert translation.get_attribute("class") == ""
