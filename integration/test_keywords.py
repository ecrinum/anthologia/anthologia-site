def test_submitting_passage_keyword_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg7000.tlg001.ag:7.71/")
    page.hover("#keywords h2")
    page.click("#keywords h2 a >> text='Add'")
    modal = page.wait_for_selector("#passage-keyword-create")
    keyword_select = modal.wait_for_selector("#id_keyword")
    keyword_select.select_option(label="Apollo, Apollon, Phébus / Phœbus")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.


def test_submitting_scholium_keyword_form(page):
    page.goto("/passages/urn:cts:greekLit:tlg5011.tlg001.sag:7.70.1/")
    page.hover("#keywords h2")
    page.click("#keywords h2 a >> text='Add'")
    modal = page.wait_for_selector("#passage-keyword-create")
    keyword_select = modal.wait_for_selector("#id_keyword")
    keyword_select.select_option(label="Apollo, Apollon, Phébus / Phœbus")
    submit_button = modal.wait_for_selector("button[type=submit]")
    # submit_button.click()
    # TODO: find the correct Playwright assertion in this context.
