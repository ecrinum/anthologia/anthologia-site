# Anthology Django

## Deployed instances

Production (`production` branch, manual):
https://anthologiagraeca.org/

Pre-production (`preprod` branch, via gitlab CI):
http://antholont.ecrituresnumeriques.ca/


## Requirements

* Python 3 (~ 3.8)

## Setup

### Development deployment

#### Install Database (Postgres) and db management
* Install PostgreSQL + PostGIS + connector : `sudo apt install libpq-dev postgresql postgresql-contrib postgis`
* Connect to database :
```
    sudo su postgres (login as the user "postgres", if normal user havent the right to access the database)
    psql
```

* Create database ([doc](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04)) and configure for django:
 ```
    CREATE DATABASE anthology;
    CREATE USER anthology_django WITH PASSWORD 'password';
    ALTER ROLE anthology_django SET client_encoding TO 'utf8';
    ALTER ROLE anthology_django SET default_transaction_isolation TO 'read committed';
    ALTER ROLE anthology_django SET timezone TO 'UTC-4';
    ALTER USER anthology_django CREATEDB;
    GRANT ALL PRIVILEGES ON DATABASE anthology TO anthology_django;
    CREATE EXTENSION postgis;
```

* (Optional) Drop database
When updating the schema, before running `makemigration` having no data yet, you want to drop existing db to 
recreate it.

 ```
    psql
    DROP DATABASE anthology;
    CREATE DATABASE anthology;
    GRANT ALL PRIVILEGES ON DATABASE anthology TO anthology_django;
```


* ONLY FOR TESTING ENV:
```
ALTER ROLE anthology_django SUPERUSER;
```

#### Setup Python environment

* In Windows: `py -3.8 -m venv venv`
* In Linux/macOS: `python3.8 -m venv venv`

> This command MUST be run with Python 3.8.

Activate the virtual environment:
* In Windows (Powershell): `.\venv\Scripts\activate.ps1` (`activate.bat` in the command line)
* Linux / macOS: `source venv/bin/activate`

Install the dependencies:
* `pip install -r requirements.txt`

#### Running server

 * Activate the virtual environment/
 * (first time) `python manage.py migrate`
 * (first time) `python manage.py import_ap`
 * `python manage.py runserver 0.0.0.0:8000`

#### Updating settings.py to run in localhost or inside Docker

Change the database in `anthology/settings.py` :

##### Docker version

> Do not change anything. The default 'db' host matches the PostgreSQL container.

##### Localhost version
In this case, you have to have a running instance of Postgres (see section above)

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'anthology',
        'USER': 'anthology_django',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}
```
 
#### Updating the model

* make change in the `anthology/api/models` folder
* run `python manage.py makemigrations`
* run `python manage.py migrate`

##### To test it in docker :

* set settings.py to use docker
* (in the case of no data has been put in db yet)
    * stop the docker-compose : `docker-compose stop`
    * delete old containers : `docker-compose rm`
* `docker-compose up -d --build`
* `docker-compose exec web python manage.py migrate`  



### Manual installation (macOS)

Check out code from the git repository:

    $ git clone git@framagit.org:anthologie-palatine/anthologyontology.git
    $ cd anthologyontology

Switch to the `preprod` branch (or create a new dedicated branch for your developments).

Setup a virtualenv and install dependencies:

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

Install and launch PostgreSQL (with homebrew, YMMV):

    $ brew install postgis
    $ brew services start postgresql

You can check the database is ready:

    $ pg_isready

Set useful env variables:

    $ export POSTGRES_DB=anthology
    $ export POSTGRES_USER=anthology_django
    $ export POSTGRES_PASSWORD=password

Create relevant user and database:

    $ createuser $POSTGRES_USER
    $ createdb $POSTGRES_DB --owner=$POSTGRES_USER

You must be an admin (`david` in my case) to perform these operations:

    $ psql --username "david" --dbname "$POSTGRES_DB"
    =# ALTER USER "anthology_django" CREATEDB;
    =# CREATE EXTENSION postgis;

You can check the list of DBs with `\l` and roles with `\du`.

Set database options:

    $ psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB"
    => ALTER ROLE "anthology_django" SET client_encoding TO 'utf8';
    => ALTER ROLE "anthology_django" SET default_transaction_isolation TO 'read committed';
    => ALTER ROLE "anthology_django" SET timezone TO 'UTC-4';
    => GRANT ALL PRIVILEGES ON DATABASE "anthology" TO "anthology_django";

Download the latest DB dump from https://gitlab.huma-num.fr/ecrinum/BaseAP/ and load it:

    $ psql $POSTGRES_DB < ap_db_2021….sql

Optionally run migrations (if any new ones on your branch):

    $ python django/manage.py migrate

Create an editor user to interact with your local instance:

    $ python django/manage.py shell_plus
    >>> user = User.objects.create_user(username='david', email='david@example.org', password='password')
    >>> perm_can_add = Permission.objects.get(codename="can_add_mlgr")
    >>> perm_own_passages = Permission.objects.get(codename="manage_own_passages")
    >>> perm_own_mlgr = Permission.objects.get(codename="manage_own_mlgr")
    >>> editors, _ = Group.objects.get_or_create(name="Editors")
    >>> editors.permissions.add(perm_own_passages, perm_own_mlgr, perm_can_add)
    >>> user.groups.add(editors)

Reset the admin password to be able to access the administration interface:

    $ python django/manage.py shell_plus
    >>> admin = User.objects.all()[0]
    >>> admin.set_password("password")
    >>> admin.save()

You should be good to go!

    $ python django/manage.py runserver
