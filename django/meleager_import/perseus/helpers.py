import itertools
from collections import namedtuple

import requests
from lxml import html

from django.conf import settings

from django.db.models import CharField
from django.db.models import Value as V
from django.db.models.functions import Concat
from meleager.models import Book, Passage

BASE_URL = "http://www.perseus.tufts.edu/hopper/dltext?doc=Perseus%3Atext%3A2008.01.047"

DATA_FOLDER = settings.BASE_DIR / "meleager_import" / "data" / "perseus"

if not DATA_FOLDER.exists():
    DATA_FOLDER.mkdir(parents=True)

Epigram = namedtuple("Epigram", ("book", "fragment"))
Epigram.__str__ = lambda self: f"{self.book}.{self.fragment}"

POSSIBLE_BOOKS = set(
    Book.objects.all().values_list("number", flat=True)
)

ALL_AP_EPIGRAMS = {
    Epigram(book, fragment=fragment)
    for (book, fragment) in Passage.objects.all()
    .annotate(
        fragment_full=Concat("fragment", "sub_fragment", output_field=CharField())
    )
    .values_list("book__number", "fragment_full")
}


def get_perseus_epigrams(display=False):
    ALL_PERSEUS_EPIGRAMS = set()
    all_epigrams = list()

    for volume in range(2, 7):
        try:
            with open(DATA_FOLDER / f"VOL_{volume}", "r") as fp:
                content = fp.read().encode()
        except FileNotFoundError:
            req = requests.get(f"{BASE_URL}{volume}")
            content = req.content
            with open(DATA_FOLDER / f"VOL_{volume}", "w") as fp:
                fp.write(content.decode())

        tree = html.fromstring(content)
        books = tree.xpath('//div1[@type="book"]/@n')
        epigrams_nested = [
            [
                Epigram(book=int(book), fragment=fragment)
                for fragment in tree.xpath(
                    f"//div1[@n='{book}']/div2[@type='chapter']/@n"
                )
            ]
            for book in books
        ]

        epigrams = {
            epigram
            for epigram in itertools.chain(*epigrams_nested)
        }

        ALL_PERSEUS_EPIGRAMS = ALL_PERSEUS_EPIGRAMS.union(epigrams)

        epigrams_to_add = epigrams - ALL_AP_EPIGRAMS
        epigrams_already_there = ALL_AP_EPIGRAMS & epigrams
        if epigrams_already_there and display:
            print(
                "We are not going to import:",
                ", ".join(map(str, epigrams_already_there)),
            )

        all_epigrams.extend(epigrams_to_add)

    # for epigram in all_epigrams:
    #   text, author, tlg = _get_epigram(epigram)
    if display:
        print(
            "IN AP BUT NOT IN PERSEUS\n",
            list(map(str, ALL_AP_EPIGRAMS - ALL_PERSEUS_EPIGRAMS)),
        )
        print("Found", len(all_epigrams), "epigrams that do not exist in our database.")

    return all_epigrams


def get_epigram(epigram):
    print("Importing epigram:", epigram.book, epigram.fragment)
    XML_URL = "http://www.perseus.tufts.edu/hopper/xmlchunk?doc=Perseus%3Atext%3A2008.01.047{index}%3Abook%3D{book}%3Achapter%3D{fragment}"

    if epigram.book in (10, 11, 12):
        index = 5
    elif epigram.book in (1, 2, 3, 4, 5, 6):
        index = 2
    elif epigram.book in (7, 8):
        index = 3
    elif epigram.book in (13, 14, 15, 16):
        index = 6
    elif epigram.book == 9:
        index = 4
    else:
        raise RuntimeError("Book not found: %s" % epigram.book)

    url = XML_URL.format(index=index, book=epigram.book, fragment=epigram.fragment)
    try:
        with open(DATA_FOLDER / f"xml_{index}_{epigram.book}_{epigram.fragment}", "r") as fp:
            content = fp.read().encode()
    except FileNotFoundError:
        resp = requests.get(url)
        content = resp.content
        with open(DATA_FOLDER / f"xml_{index}_{epigram.book}_{epigram.fragment}", "w") as fp:
            fp.write(content.decode())

    tree = html.fromstring(content)

    text = "".join(tree.xpath("//text//p//text()")).strip()
    author_list = tree.xpath("//text//persname//text()")
    author = None
    tlg = None

    if len(author_list):
        author = author_list[0]
        tlg = tree.xpath("//text//persname/@key")[0]

    return (text, author, tlg)
