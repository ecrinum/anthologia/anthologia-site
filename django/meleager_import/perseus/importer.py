import re
from meleager.models import Passage, Author, Name, Book, Text, Work
from .helpers import get_epigram

def import_epigram(epigram):
    text, author, tlg = get_epigram(epigram)
    txt_obj = Text.objects.create(
        text=text,
        language_id='grc',
    )

    author_obj = None
    if tlg:
        author_obj, created = Author.objects.get_or_create(tlg_id=tlg)
        if created:
            author_obj.names.add(Name.objects.update_or_create(name=author, language_id="grc")[0])

    # See #146 - all texts are part of Work "Anthologie Grecque"
    # work = Work.objects.filter(descriptions__description__icontains="anthology").first()
    work = Work.objects.first()
    book, _ = Book.objects.get_or_create(number=epigram.book, defaults={'work': work})

    matches = re.match(r"(\d+)(\w*)", epigram.fragment)
    fragment, sub_fragment = matches.groups()
    passage = Passage.objects.create(book=book, fragment=fragment, sub_fragment=sub_fragment)
    passage.texts.add(txt_obj)

    if author_obj:
        passage.authors.add(author_obj)

