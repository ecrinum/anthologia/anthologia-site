import logging

from meleager.models import Keyword, KeywordCategory, Name

from ..constants import API_URL, LANG_API2CODE
from ..helpers import to_date
from .base_manager import BaseManager
from .data_manager import DataManager

logger = logging.getLogger(__name__)


class KeywordCategoryFromApi(BaseManager):
    @classmethod
    def _create_object(cls, id_category, json_data):
        name_obj = Name.objects.create(name=json_data["title"], language_id="fra")
        cls.update_obj_user_dates_from_json(name_obj, json_data)

        cat_obj, created = KeywordCategory.objects.get_or_create(unique_id=id_category)
        cat_obj.names.add(name_obj)
        cls.update_obj_user_dates_from_json(cat_obj, json_data)

        action = "NEW" if created else "OK"

        logger.info("%s | Keyword PK %s (AP %s)", action, cat_obj.pk, id_category)

        return cat_obj


class KeywordFromApi(BaseManager):
    @classmethod
    def _create_object(cls, id_keyword, json_data):
        json_data = DataManager.get(f"{API_URL}/v1/keywords/{id_keyword}")
        names = list()
        for version in json_data["versions"]:
            name_obj = Name.objects.create(
                name=version["title"], language_id=LANG_API2CODE[version["id_language"]]
            )
            cls.update_obj_user_dates_from_json(name_obj, version)
            names.append(name_obj)

        keyword_obj, created = Keyword.objects.get_or_create(unique_id=id_keyword)
        keyword_obj.names.add(*names)
        cls.update_obj_user_dates_from_json(keyword_obj, json_data)

        action = "NEW" if created else "OK"

        logger.info("%s | Keyword PK %s (AP %s)", action, keyword_obj.pk, id_keyword)

        cat_obj = KeywordCategoryFromApi(
            key=None, json_data=json_data["category"], key_name="id_keyword_category"
        )
        cat_obj.keywords.add(keyword_obj)

        return keyword_obj

    @classmethod
    def _create_all(cls):
        json_data = DataManager.get(f"{API_URL}/v1/keywords/")
        for kw in json_data:
            cls._create_object(kw["id_keyword"])
