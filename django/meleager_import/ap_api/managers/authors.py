import csv
from urllib.parse import urlparse

from django.conf import settings
from meleager.models import URN, Author, Name

from ..constants import API_URL, LANG_API2CODE
from ..helpers import to_date
from .cities import CityFromApi
from .data_manager import DataManager
from .users import UserFromApi

data_file = (
    settings.BASE_DIR
    / "meleager_import"
    / "data"
    / "authors_tlg.csv"
)

with open(data_file, "r", newline="") as csvfile:
    has_header = csv.Sniffer().has_header(csvfile.read(1024))
    csvfile.seek(0)
    reader = csv.reader(csvfile)
    lines = [row for row in reader]
    if has_header:
        lines.pop(0)

    PERSEUS_AUTHORS = {
        int(row[0]): {"tlg": row[3].strip(), "wiki_id": row[5].strip()} for row in lines
    }


class AuthorFromApi:
    _authors = dict()

    def __new__(cls, id_author):
        # Check if author exists etc
        try:
            author_obj = Author.objects.get(unique_id=id_author)
        except Author.DoesNotExist:
            author_obj = cls._create_author(id_author)
        else:
            if id_author not in cls._authors:
                cls._authors[id_author] = author_obj
            else:
                author_obj = cls._authors[id_author]

        return author_obj

    @classmethod
    def _create_author(cls, id_author):
        json_data = DataManager.get(f"{API_URL}/v1/authors/{id_author}")
        user_obj = UserFromApi(json_data["id_user"]["id_user"])

        # Create names
        names = [
            Name.objects.create(
                name=version["name"].strip(),
                language_id=LANG_API2CODE[version["id_language"]],
                creator=user_obj,
                last_editor=user_obj,
                created_at=to_date(version["createdAt"]),
                updated_at=to_date(version["updatedAt"]),
            )
            for version in json_data["versions"]
        ]

        city_born = CityFromApi(json_data.get("city_born"))
        city_died = CityFromApi(json_data.get("city_died"))

        author_data = {
            "unique_id": id_author,
            "city_born": city_born,
            "city_died": city_died,
        }

        urns = list()

        if id_author in PERSEUS_AUTHORS:
            author_data.update(tlg_id=PERSEUS_AUTHORS[id_author]["tlg"])
            wiki_urn, _ = URN.objects.update_or_create(
                urn=PERSEUS_AUTHORS[id_author]["wiki_id"]
            )
            urns.append(wiki_urn)

        author_obj = Author.objects.create(**author_data)

        author_obj.names.add(*names)
        author_obj.save()
        author_obj.alternative_urns.add(*urns)

        return author_obj
