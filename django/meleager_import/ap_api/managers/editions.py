import logging

from meleager.models import Description, Edition

logger = logging.getLogger(__name__)


class EditionFromApi:
    _editions = dict()

    def __new__(cls, edition, id_language, work_obj_pk):
        edition_name = cls.get_edition_name(edition, id_language)

        if not edition_name:
            if edition:
                edition_name = edition
            else:
                return None

        if edition_name not in cls._editions:
            edition_obj = Edition.objects.filter(descriptions__description=edition_name)
            if not edition_obj:
                name = Description.objects.create(
                    description=edition_name, language_id="eng"
                )
                edition_obj = Edition.objects.create(
                    work_id=work_obj_pk, edition_type=Edition.EditionType.MANUSCRIPT
                )
                edition_obj.descriptions.add(name)
                cls._editions[edition_name] = edition_obj
                action = "NEW"
            else:
                edition_obj = edition_obj.first()
                cls._editions[edition_name] = edition_obj
                action = "OK"

            logger.info(
                "%s | Edition %s", action, edition_obj.descriptions.first().description
            )
        else:
            edition_obj = cls._editions[edition_name]

        return edition_obj

    @staticmethod
    def get_edition_name(orig_edition, id_language):
        edition_name = None
        if orig_edition is not None:
            if orig_edition in [
                "W.R. Paton ",
                "Paton",
                "W.R. Paton",
                "W. R; Paton",
                "Paton ",
                "W. R. Paton",
                "W. R Paton",
                "Perseus",
                "W. R. Paton ",
                "W. R. Paton (modified)",
                "W.R Paton",
                "Paton - latin",
            ]:
                edition_name = "Paton edition"
            elif orig_edition in [
                "W.R. Paton (modifié)",
                "Paton (modified)",
                "W.R. Paton (modified)",
                "W, R. Paton (modified)",
                "Paton modified",
                "Paton modified ",
            ]:
                edition_name = "Paton modified"
            elif orig_edition in [
                "W.R. Paton, by Ulysse Bouchard",
                "W. R. Paton, by Ulysse Bouchard",
            ]:
                edition_name = "Paton modified by Ulysse Bouchard"
            elif orig_edition == "Ulysse Bouchard":
                edition_name = "Ulysse Bouchard"
            elif orig_edition in [
                "Waltz ",
                "Waltz",
                "P.  Waltz",
                "W. R. Waltz",
                "P. Waltz",
                "P. Waltz (modifié)",
            ]:
                edition_name = "Waltz edition"
            elif orig_edition in [
                "Marcello",
                "marviro",
                "Marcello Vitali-Rosati",
                "Marcello - Palatinus",
            ]:
                edition_name = "Marcello Vitali-Rosati"
            elif orig_edition in ["Fernando Pessoa ", "Fernando Pessoa"]:
                edition_name = "Fernando Pessoa"
            elif orig_edition in ["Luiz Capelo ", "Luiz Capelo"]:
                edition_name = "Luiz Capelo"
            elif orig_edition in ["(Elsa)", "Elsa"]:
                edition_name = "Elsa Bouchard"
            elif orig_edition in ["W.R. Paton (modifié par Maxime)"]:
                edition_name = "Paton modified by Maxime"
        elif id_language == 7:
            edition_name = "Cagnazzi"
        elif id_language == 8:
            edition_name = "Paton edition"
        elif id_language == 9:
            edition_name = "Liceo Majorana"
        elif id_language == 10:
            edition_name = "Katerina"

        return edition_name
