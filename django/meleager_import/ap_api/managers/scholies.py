import logging

from meleager.models import Scholium, URN

from ..constants import API_URL, ANTHOLOGIA_URN_SOURCE
from ..helpers import to_date
from .data_manager import DataManager
from .media import ImageFromApi, ManuscriptFromApi
from .texts import ScholiumTextFromApi
from .users import UserFromApi

logger = logging.getLogger(__name__)


class ScholiumFromApi:
    _scholies = dict()

    def __new__(cls, id_scholie, passage_pk):
        if id_scholie not in cls._scholies:
            scholium_obj = cls._create_scholium(id_scholie, passage_pk)
            logger.info("NEW | Scholium PK %s (AP API %s)", scholium_obj.pk, id_scholie)
        else:
            scholium_obj = cls._scholies[id_scholie]

        return scholium_obj

    @classmethod
    def _create_scholium(cls, id_scholie, passage_pk):
        json_data = DataManager.get(f"{API_URL}/v1/scholies/{id_scholie}")
        user_obj = UserFromApi(json_data["id_user"]["id_user"])
        scholium_obj = Scholium.objects.create(
            unique_id=id_scholie,
            passage_id=passage_pk,
            number=json_data["title"].rsplit(".", 1)[-1],
            creator=user_obj,
            last_editor=user_obj,
            created_at=to_date(json_data["createdAt"]),
            updated_at=to_date(json_data["updatedAt"]),
        )

        anthologia_urn, _ = URN.objects.update_or_create(
            urn=f"{API_URL}/v1/scholies/{id_scholie}", defaults={"source": ANTHOLOGIA_URN_SOURCE}
        )
        scholium_obj.alternative_urns.add(anthologia_urn)

        for text in json_data["versions"]:
            text_obj = ScholiumTextFromApi(text)
            scholium_obj.texts.add(text_obj)

        for image in json_data["images"]:
            image_obj = ManuscriptFromApi(
                key=None, json_data=image, key_name="id_image"
            )
            scholium_obj.manuscripts.add(image_obj)

        for manuscript in json_data.get("imagesManuscript", []):
            image_obj = ManuscriptFromApi(
                key=None, json_data=manuscript, key_name="id_image"
            )
            scholium_obj.manuscripts.add(image_obj)

        return scholium_obj
