# from django.contrib.gis.geos import Point

from meleager.models import City, Name, URN

from ..constants import API_URL, LANG_API2CODE, ANTHOLOGIA_URN_SOURCE
from ..helpers import to_date
from .data_manager import DataManager
from .users import UserFromApi


class CityFromApi:
    _cities = dict()

    def __new__(cls, json_data):
        if json_data is None:
            return None

        if json_data["id_city"] == 0:
            return None

        if json_data["id_city"] not in cls._cities:
            city_obj = cls._create_city(json_data["id_city"])
            cls._cities[json_data["id_city"]] = city_obj
        else:
            city_obj = cls._cities[json_data["id_city"]]

        return city_obj

    @classmethod
    def _create_city(cls, id_city):
        json_data = DataManager.get(f"{API_URL}/v1/cities/{id_city}")
        user_obj = UserFromApi(json_data["id_user"]["id_user"])
        names = [
            Name.objects.create(
                name=version["name"],
                language_id=LANG_API2CODE[version["id_language"]],
                creator=user_obj,
                last_editor=user_obj,
                created_at=to_date(version["createdAt"]),
                updated_at=to_date(version["updatedAt"]),
            )
            for version in json_data["versions"]
        ]

        longitude = latitude = None

        if json_data["GPS"] is not None:
            coords = json_data["GPS"].split(",")
            if len(coords) != 2:
                coords = json_data["GPS"].split(" ")

            try:
                longitude, latitude = (float(c) for c in coords)
            except ValueError:
                pass

        city_obj = City.objects.create(
            unique_id=id_city, longitude=longitude, latitude=latitude
        )
        anthologia_urn, _ = URN.objects.update_or_create(
            urn=f"{API_URL}/v1/cities/{id_city}", defaults={"source": ANTHOLOGIA_URN_SOURCE}
        )
        city_obj.names.add(*names)
        city_obj.alternative_urns.add(anthologia_urn)

        return city_obj
