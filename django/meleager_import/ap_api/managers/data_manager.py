import json
import os
from pathlib import Path
from urllib.parse import urlparse

import requests

from ..constants import IMPORT_DIR


class DataManager:
    @classmethod
    def init(cls, filepath=None):
        if not Path(IMPORT_DIR).exists():
            Path(IMPORT_DIR).mkdir(exist_ok=True)

    @classmethod
    def get(cls, url):
        filename = Path(IMPORT_DIR) / cls._url_to_filename(url)

        if not Path(filename).exists():
            data = requests.get(url).json()
            with open(filename, "w") as fp:
                json.dump(data, fp)
            return data
        else:
            with open(filename, "r") as fp:
                return json.load(fp)

    @staticmethod
    def _url_to_filename(url):
        components = urlparse(url)
        return components.path.replace("/", "__")


DataManager.init()
