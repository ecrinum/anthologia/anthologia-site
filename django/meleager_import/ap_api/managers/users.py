import logging

from meleager_user.models import User

from ..constants import API_URL
from .data_manager import DataManager

logger = logging.getLogger(__name__)


class UserFromApi:
    _users = dict()

    def __new__(cls, user_id):
        if not user_id:
            return None

        if user_id not in cls._users:
            cls._create_all_users()

        user_obj = cls._users[user_id]
        return user_obj

    @classmethod
    def _create_all_users(cls):
        json_data = DataManager.get(f"{API_URL}/v1/users/")
        for user_json in json_data:
            # Find the user
            user_obj = User.objects.filter(username=user_json["displayName"])

            if user_obj:
                user_obj = user_obj.first()
                action = "OK"
            else:
                user_obj = User.objects.create_user(username=user_json["displayName"])
                action = "NEW"

            # Update user if necessary
            User.objects.filter(pk=user_obj.pk).update(
                first_name=user_json.get("first_name", ""),
                last_name=user_json.get("last_name", ""),
                institution=user_json.get("institution", ""),
            )

            logger.info("%s | User %s", action, user_obj.username)

            # Add the user to class dictionary
            cls._users[user_json["id_user"]] = user_obj
