import logging
import re

from meleager.models import (URN, Alignment, Author, Language, Book, City, Comment, Description,
                             Edition, Keyword, KeywordCategory, Manuscript,
                             Medium, Name, Passage, Scholium, Text, Work)

from .constants import API_URL
from .managers.data_manager import DataManager
from .managers.languages import create_languages
from .managers.passages import PassageFromApi

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format="%(message)s")


def import_all():
    # DELETE EVERYTHING
    URN.objects.all().delete()
    Author.objects.all().delete()
    Comment.objects.all().delete()
    Description.objects.all().delete()
    Name.objects.all().delete()
    Alignment.objects.all().delete()
    Medium.objects.all().delete()
    Manuscript.objects.all().delete()
    Keyword.objects.all().delete()
    KeywordCategory.objects.all().delete()
    Book.objects.all().delete()
    Work.objects.all().delete()
    Passage.objects.all().delete()
    Scholium.objects.all().delete()
    City.objects.all().delete()
    Text.objects.all().delete()
    Edition.objects.all().delete()

    # Update languages
    if Language.objects.count() < 10:
        create_languages()

    # CREATE PASSAGES
    passages = DataManager.get(f"{API_URL}/v1/entities/")
    for passage in passages:
        print("="*80)
        print("Importing passage:", passage["id_entity"])
        PassageFromApi(passage["id_entity"])

def import_one(passage_id):
    try:
        obj = Passage.objects.get(unique_id=passage_id)
        obj.texts.all().delete()
        obj.comments.all().delete()
        obj.delete()
    except Passage.DoesNotExist:
        pass

    PassageFromApi(passage_id)