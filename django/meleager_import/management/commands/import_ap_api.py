from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.db.models.aggregates import Count
from meleager.models import Language, Author

from meleager_import.ap_api import import_all, import_one


class Command(BaseCommand):
    help = "Imports AP data into Django DB - v2"

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('passage_id', nargs='?', type=int)

    def handle(self, *args, **options):
        call_command("migrate")
        if Language.objects.count() <= 4:
            call_command("loaddata", "languages")

        if options["passage_id"]:
            import_one(options["passage_id"])
        else:
            import_all()
