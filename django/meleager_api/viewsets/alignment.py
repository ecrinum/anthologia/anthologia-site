from rest_framework import viewsets

from meleager.models import Alignment
from ..serializers.alignment import AlignmentSerializer
from ..pagination import ApiPagination


class AlignmentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Alignment.objects.all()
    serializer_class = AlignmentSerializer
    pagination_class = ApiPagination
