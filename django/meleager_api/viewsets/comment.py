from rest_framework import viewsets

from meleager.models import Comment

from ..serializers.comment import CommentSerializer
from ..pagination import ApiPagination


class CommentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    pagination_class = ApiPagination
