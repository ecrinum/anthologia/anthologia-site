from rest_framework import serializers

from meleager.models import Name


class MinimalLinkedNameSerializer(serializers.ModelSerializer):
    language = serializers.CharField(source="language.code")

    class Meta:
        model = Name
        fields = ('name', 'language')
