from rest_framework import serializers

from meleager.models import Language


class FullLanguageSerializer(serializers.HyperlinkedModelSerializer):
    counts = serializers.SerializerMethodField()

    def get_counts(self, obj):
        texts = obj.text_set.all().count()
        descriptions = obj.description_set.all().count()
        names = obj.name_set.all().count()
        return {
            "texts": texts,
            "descriptions": descriptions,
            "names": names,
        }

    class Meta:
        model = Language
        fields = ('code', 'iso_name', 'preferred', 'url', 'counts')


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('code', 'iso_name', 'url')
