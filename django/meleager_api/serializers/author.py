from rest_framework import serializers

from meleager.models import Author
from .name import MinimalLinkedNameSerializer
from .city import CitySerializer
from .passage import PassageHyperlink


class NumericRange(serializers.Field):
    def to_representation(self, value):
        return {"lower": value.lower, "upper": value.upper}


class UrnRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        alternative_urns = value.urn
        return alternative_urns


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    names = MinimalLinkedNameSerializer(many=True)
    alternative_urns = UrnRelatedField(
        many=True,
    )
    city_born = CitySerializer()
    city_died = CitySerializer()
    born_range_year_date = NumericRange()
    died_range_year_date = NumericRange()
    passages = PassageHyperlink(many=True, read_only=True, view_name="passage-detail")

    class Meta:
        model = Author
        fields = [
            "id",
            "url",
            "names",
            "alternative_urns",
            "city_born",
            "city_died",
            "born_range_year_date",
            "died_range_year_date",
            "unique_id",
            "created_at",
            "updated_at",
            "validation",
            "tlg_id",
            "main_name",
            "descriptions",
            "images",
            "passages",
        ]
