from django.urls import path

from ..views.manuscripts import (
    PassageManuscriptCreate,
    PassageManuscriptDelete,
    ScholiumManuscriptCreate,
    ScholiumManuscriptDelete,
)

passage_patterns = [
    path(
        "create/",
        PassageManuscriptCreate.as_view(),
        name="passage-manuscript-create",
    ),
    path(
        "delete/<int:manuscript_pk>/",
        PassageManuscriptDelete.as_view(),
        name="passage-manuscript-delete",
    ),
]
scholium_patterns = [
    path(
        "create/",
        ScholiumManuscriptCreate.as_view(),
        name="scholium-manuscript-create",
    ),
    path(
        "delete/<int:manuscript_pk>/",
        ScholiumManuscriptDelete.as_view(),
        name="scholium-manuscript-delete",
    ),
]
