from django.urls import include, path
from web.views.home import home

from .authors import urlpatterns as authors_urls
from .cities import urlpatterns as cities_urls
from .keywords import urlpatterns as keywords_urls
from .passages import urlpatterns as passages_urls
from .scholia import urlpatterns as scholia_urls
from .wikidata import urlpatterns as wikidata_urls
from .history import urlpatterns as history_urls
from .pages import urlpatterns as pages_urls

app_name = "web"

urlpatterns = [
    path("", home, name="index"),
    path("cities/", include(cities_urls)),
    path("passages/", include(passages_urls)),
    path("scholia/", include(scholia_urls)),
    path("authors/", include(authors_urls)),
    path("keywords/", include(keywords_urls)),
    path("wikidata/", include(wikidata_urls)),
    path("history/", include(history_urls)),
    path("pages/", include(pages_urls)),
    path("i18n/", include("django.conf.urls.i18n")),
]
