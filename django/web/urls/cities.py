from django.urls import path
from web.views.cities import (
    CityCreateNames,
    CityCreateWikidataURL,
    CityDetail,
    CityList,
    PassageCityCreate,
    PassageCityDelete,
    ScholiumCityCreate,
    ScholiumCityDelete,
)

urlpatterns = [
    path("", CityList.as_view(), name="city-list"),
    path("<int:pk>/", CityDetail.as_view(), name="city-detail"),
    path("create/", CityCreateNames.as_view(), name="city-create-names"),
    path(
        "create/wikidata/",
        CityCreateWikidataURL.as_view(),
        name="city-create-wikidata",
    ),
]

passage_patterns = [
    path("create/", PassageCityCreate.as_view(), name="passage-city-create"),
    path(
        "delete/<int:city_pk>/", PassageCityDelete.as_view(), name="passage-city-delete"
    ),
]

scholium_patterns = [
    path("create/", ScholiumCityCreate.as_view(), name="scholium-city-create"),
    path(
        "delete/<int:city_pk>/",
        ScholiumCityDelete.as_view(),
        name="scholium-city-delete",
    ),
]
