from django.urls import include, path

from .cities import scholium_patterns as cities_urls
from .comments import scholium_patterns as comments_urls
from .descriptions import scholium_patterns as descriptions_urls
from .keywords import scholium_patterns as keywords_urls
from .manuscripts import scholium_patterns as manuscripts_urls
from .texts import scholium_patterns as texts_urls

urlpatterns = [
    path("<int:scholium_pk>/descriptions/", include(descriptions_urls)),
    path("<int:scholium_pk>/texts/", include(texts_urls)),
    path("<int:scholium_pk>/cities/", include(cities_urls)),
    path("<int:scholium_pk>/keywords/", include(keywords_urls)),
    path("<int:scholium_pk>/comments/", include(comments_urls)),
    path("<int:scholium_pk>/manuscripts/", include(manuscripts_urls)),
]
