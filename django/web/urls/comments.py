from django.urls import path
from web.views.comments import (PassageCommentCreate, PassageCommentDelete,
                                PassageCommentUpdate, ScholiumCommentCreate,
                                ScholiumCommentDelete, ScholiumCommentUpdate)

passage_patterns = [
    path("create/", PassageCommentCreate.as_view(), name="passage-comment-create"),
    path(
        "update/<int:comment_pk>/",
        PassageCommentUpdate.as_view(),
        name="passage-comment-update",
    ),
    path(
        "delete/<int:comment_pk>/",
        PassageCommentDelete.as_view(),
        name="passage-comment-delete",
    ),
]

scholium_patterns = [
    path("create/", ScholiumCommentCreate.as_view(), name="scholium-comment-create"),
    path(
        "update/<int:comment_pk>/",
        ScholiumCommentUpdate.as_view(),
        name="scholium-comment-update",
    ),
    path(
        "delete/<int:comment_pk>/",
        ScholiumCommentDelete.as_view(),
        name="scholium-comment-delete",
    ),
]
