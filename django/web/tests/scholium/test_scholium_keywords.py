from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(scholium_1):
    return reverse("web:scholium-keyword-create", args=(scholium_1.pk,))


@pytest.fixture
def delete_url(scholium_1, keyword_1):
    return reverse("web:scholium-keyword-delete", args=(scholium_1.pk, keyword_1.pk))


@pytest.fixture
def keyword_scholium_1(scholium_1, keyword_1, editor):
    from meleager.models import KeywordScholium

    keyword_scholium = KeywordScholium.objects.create(
        user=editor, scholium_id=scholium_1.pk, keyword_id=keyword_1.pk
    )

    return keyword_scholium


@pytest.fixture
def keyword_payload(keyword_1):
    return {"keyword": keyword_1.pk}


class TestScholiumKeywordAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, keyword_payload):
        response = client.post(create_url, keyword_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url, keyword_scholium_1):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url, keyword_scholium_1):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestScholiumKeywordEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(
        self, client_editor, delete_url, keyword_scholium_1
    ):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_keyword(
        self,
        client_editor,
        create_url,
        keyword_payload,
        scholium_1,
        keyword_1,
        language_french,
    ):
        from meleager.models import Keyword

        assert scholium_1.keywords.count() == 0
        response = client_editor.post(create_url, keyword_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#keywords"
        )
        associated_keyword = Keyword.objects.get(
            names__name=keyword_1.names.all().first().name
        )
        assert associated_keyword in scholium_1.keywords.all()

    def test_create_keyword_generate_versions(
        self, client_editor, create_url, keyword_payload, keyword_1, scholium_1, editor
    ):
        response = client_editor.post(create_url, keyword_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Keyword&pk={keyword_1.pk}&action_type=associate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_keyword(
        self, client_editor, delete_url, keyword_1, scholium_1, keyword_scholium_1
    ):
        assert keyword_1 in scholium_1.keywords.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == "/passages/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.1/#keywords"
        )
        assert keyword_1 not in scholium_1.keywords.all()

    def test_delete_keyword_generate_versions(
        self,
        client_editor,
        delete_url,
        keyword_1,
        scholium_1,
        keyword_scholium_1,
        editor,
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(scholium_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Keyword&pk={keyword_1.pk}&action_type=deassociate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_keyword_if_not_creator(
        self,
        client_editor,
        delete_url,
        keyword_1,
        scholium_1,
        keyword_scholium_1,
        regular,
    ):
        assert keyword_1 in scholium_1.keywords.all()
        keyword_scholium_1.user = regular
        keyword_scholium_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert keyword_1 in scholium_1.keywords.all()
