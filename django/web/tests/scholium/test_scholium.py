from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def passage_detail_url(book_1, passage_1):
    return reverse(
        "web:passage-detail",
        args=(book_1.number, passage_1.fragment, passage_1.sub_fragment),
    )


@pytest.fixture
def scholium_detail_url(book_1, passage_1, scholium_1):
    return reverse(
        "web:scholium-detail",
        args=(
            book_1.number,
            passage_1.fragment,
            passage_1.sub_fragment,
            scholium_1.number,
        ),
    )


@pytest.fixture
def scholium_create_url(book_1, passage_1):
    return reverse("web:scholium-create", args=(passage_1.pk,))


@pytest.fixture
def scholium_payload_1():
    return {"number": 1}


@pytest.mark.django_db
class TestScholiumAnonymous:
    def test_can_access_scholium_page(self, client, scholium_detail_url, scholium_1):
        response = client.get(scholium_detail_url)
        assert response.status_code == HTTPStatus.OK
        assert str(scholium_1) in str(response.content)

    def test_linked_from_passage_page(
        self, client, passage_detail_url, passage_1, scholium_1
    ):
        response = client.get(passage_detail_url)
        assert response.status_code == HTTPStatus.OK
        assert str(scholium_1) == "Scholium 1.1.1"
        assert str(scholium_1) in str(response.content)

    def test_cannot_access_create_scholium_page(self, client, scholium_create_url):
        response = client.get(scholium_create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_scholium_page(
        self, client, scholium_create_url, scholium_payload_1
    ):
        response = client.post(scholium_create_url, scholium_payload_1)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestScholiumEditor:
    def test_can_access_create_scholium_page(self, client_editor, scholium_create_url):
        response = client_editor.get(scholium_create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_scholium(
        self, client_editor, scholium_create_url, scholium_payload_1, passage_1
    ):
        from meleager.models import Scholium

        assert passage_1.scholia.count() == 0

        response = client_editor.post(scholium_create_url, scholium_payload_1)

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/passages/")
        assert passage_1.scholia.count() == 1
        created_scholium = Scholium.objects.get(number=scholium_payload_1["number"])
        assert created_scholium in passage_1.scholia.all()

    def test_can_create_scholium_generate_versions(
        self, client_editor, scholium_create_url, scholium_payload_1, passage_1, editor
    ):
        from meleager.models import Scholium

        assert passage_1.scholia.count() == 0

        response = client_editor.post(scholium_create_url, scholium_payload_1)

        assert response.status_code == HTTPStatus.FOUND
        created_scholium = Scholium.objects.get(number=scholium_payload_1["number"])
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Scholium&pk={created_scholium.pk}&action_type=create&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_create_duplicate_scholium(
        self, client_editor, scholium_create_url, scholium_payload_1, passage_1
    ):
        from meleager.models import Scholium

        assert passage_1.scholia.count() == 0

        # This first request should create a Scholium (setup).
        response = client_editor.post(scholium_create_url, scholium_payload_1)

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/passages/")
        assert passage_1.scholia.count() == 1
        created_scholium = Scholium.objects.get(number=scholium_payload_1["number"])
        assert created_scholium in passage_1.scholia.all()

        # We post the same request again, should not create any
        # duplicate or modification.
        response = client_editor.post(scholium_create_url, scholium_payload_1)

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/passages/")
        assert passage_1.scholia.count() == 1
        created_scholium = Scholium.objects.get(number=scholium_payload_1["number"])
        assert created_scholium in passage_1.scholia.all()

        # Only the first version is created.
        versions = Version.objects.get_for_object(passage_1)
        assert len(versions) == 1
