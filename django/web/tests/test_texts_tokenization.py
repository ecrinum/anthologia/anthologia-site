import pytest


@pytest.fixture
@pytest.mark.django_db
def text_1(passage_1, language_english, editor):
    from meleager.models import Text

    text = Text.objects.create(
        text="Text content.", language=language_english, unique_id=1, creator=editor
    )
    passage_1.texts.add(text)
    return text


@pytest.mark.django_db
@pytest.mark.parametrize(
    "text, tokens",
    [
        (
            "foo",
            [[[{"t": "foo", "h": [], "pos": [1, 1], "parent": 0, "children": 1}], []]],
        ),
        (
            "foo bar",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With comma punctuation.
        (
            "foo, bar",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": ","},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With semi-column punctuation.
        (
            "foo; bar",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": ";"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With semi-column and unbreakable space punctuation.
        (
            "foo ; bar",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": ";"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With point followed by quotes punctuation.
        (
            "foo. » bar",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": "."},
                        {"p": "»"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With French apostrophes.
        (
            "c’était maintenant",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "c",
                        },
                        {"p": "’"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "était",
                        },
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "maintenant",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With English apostrophes.
        (
            "it's now",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "it",
                        },
                        {"p": "'"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "s",
                        },
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "now",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With dash.
        (
            "Dis-moi, chien",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "Dis",
                        },
                        {"p": "-"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "moi",
                        },
                        {"p": ","},
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "chien",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With term surrounded by French quotes.
        (
            "foo « BAR. » baz",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": "«"},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "BAR",
                        },
                        {"p": "."},
                        {"p": "»"},
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "baz",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With term surrounded by English quotes.
        (
            'foo "BAR." baz',
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {"p": '"'},
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "BAR",
                        },
                        {"p": "."},
                        {"p": '"'},
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "baz",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With a new line.
        (
            "foo bar\n baz quux",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "baz",
                        },
                        {
                            "children": 4,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 4],
                            "t": "quux",
                        },
                    ],
                    [],
                ]
            ],
        ),
        # With a new paragraph.
        (
            "foo bar\n baz quux\n\n quuz corge grault",
            [
                [
                    [
                        {
                            "children": 1,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 1],
                            "t": "foo",
                        },
                        {
                            "children": 2,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 2],
                            "t": "bar",
                        },
                    ],
                    [
                        {
                            "children": 3,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 3],
                            "t": "baz",
                        },
                        {
                            "children": 4,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 4],
                            "t": "quux",
                        },
                    ],
                    [],
                    [
                        {
                            "children": 5,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 5],
                            "t": "quuz",
                        },
                        {
                            "children": 6,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 6],
                            "t": "corge",
                        },
                        {
                            "children": 7,
                            "h": [],
                            "parent": 0,
                            "pos": [1, 7],
                            "t": "grault",
                        },
                    ],
                    [],
                ]
            ],
        ),
    ],
)
def test_text_tokenization(text, tokens, text_1):
    text_1.text = text
    assert text_1.alignment_tokens() == tokens
