import pytest


@pytest.mark.parametrize(
    "revision_comment, output",
    [
        ("foo", "foo"),
        ("initial revision", "First revision"),
    ],
)
def test_convert_revision_comment_defaults(revision_comment, output):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    assert convert_revision_comment(revision_comment) == output


def test_convert_revision_comment_m2m(city_1):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    revision_comment = f"app_label=meleager&model=City&pk={city_1.pk}&action_type=associate&relation_type=m2m"

    assert (
        convert_revision_comment(revision_comment)
        == f'Association of <a href="/cities/{city_1.pk}/">City name EN</a>'
    )


@pytest.mark.django_db
def test_convert_revision_comment_fk(passage_1, comment_1):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    passage_1.comments.add(comment_1)
    revision_comment = f"app_label=meleager&model=Comment&pk={comment_1.pk}&action_type=create&relation_type=fk"

    assert (
        convert_revision_comment(revision_comment)
        == f'Creation of <a href="/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#comment-{comment_1.pk}">Comment (PK {comment_1.pk})</a>'
    )


@pytest.mark.django_db
def test_convert_revision_comment_external_reference(passage_1, external_reference_1):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    passage_1.external_references.add(external_reference_1)
    revision_comment = f"app_label=meleager&model=ExternalReference&pk={external_reference_1.pk}&action_type=add&relation_type=fk"

    assert (
        convert_revision_comment(revision_comment)
        == f'Addition of <a href="/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#external-reference-{external_reference_1.pk}">ExtRef: ExternalReference content.</a>'
    )


@pytest.mark.django_db
def test_convert_revision_comment_author(passage_1, author_1):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    passage_1.authors.add(author_1)
    revision_comment = f"app_label=meleager&model=Author&pk={author_1.pk}&action_type=associate&relation_type=m2m"

    assert (
        convert_revision_comment(revision_comment)
        == f'Association of <a href="/authors/{author_1.pk}/">Author FR ({author_1.pk})</a>'
    )


@pytest.mark.django_db
def test_convert_revision_comment_after_deletion(passage_1, comment_1):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    passage_1.comments.add(comment_1)
    revision_comment = f"app_label=meleager&model=Comment&pk={comment_1.pk}&action_type=create&relation_type=fk"
    comment_1.delete()

    assert convert_revision_comment(revision_comment) == "Creation of Comment"


@pytest.mark.django_db
def test_convert_revision_comment_internal_reference(passage_1, passage_2):
    from meleager.templatetags.revisions_tags import convert_revision_comment

    revision_comment = f"app_label=meleager&model=Passage&pk_from={passage_1.pk}&pk_to={passage_2.pk}&action_type=associate&relation_type=self"

    assert (
        convert_revision_comment(revision_comment, passage_1)
        == f'Association of internal reference to <a href="{passage_2.get_absolute_url()}">{passage_2}</a>'
    )
    assert (
        convert_revision_comment(revision_comment, passage_2)
        == f'Association of internal reference from <a href="{passage_1.get_absolute_url()}">{passage_1}</a>'
    )
