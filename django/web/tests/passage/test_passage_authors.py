from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-author-create", args=(passage_1.pk,))


@pytest.fixture
def delete_url(passage_1, author_1):
    return reverse("web:passage-author-delete", args=(passage_1.pk, author_1.pk))


@pytest.fixture
def author_passage_1(passage_1, author_1, editor):
    from meleager.models import AuthorPassage

    author_passage = AuthorPassage.objects.create(
        user=editor, passage_id=passage_1.pk, author_id=author_1.pk
    )

    return author_passage


@pytest.fixture
def author_payload(author_1):
    return {"author": author_1.pk}


class TestPassageAuthorAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, author_payload):
        response = client.post(create_url, author_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url, author_passage_1):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url, author_passage_1):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageAuthorEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url, author_passage_1):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_author(
        self,
        client_editor,
        create_url,
        author_payload,
        passage_1,
        author_1,
        language_french,
    ):
        from meleager.models import Author

        assert passage_1.authors.count() == 0
        response = client_editor.post(create_url, author_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#authors"
        )
        associated_author = Author.objects.get(
            names__name=author_1.names.all().first().name
        )
        assert associated_author in passage_1.authors.all()

    def test_create_author_generate_versions(
        self, client_editor, create_url, author_payload, author_1, passage_1, editor
    ):
        response = client_editor.post(create_url, author_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Author&pk={author_1.pk}&action_type=associate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_author(
        self, client_editor, delete_url, author_1, passage_1, author_passage_1
    ):
        assert author_1 in passage_1.authors.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#authors"
        )
        assert author_1 not in passage_1.authors.all()

    def test_delete_author_generate_versions(
        self, client_editor, delete_url, author_1, passage_1, author_passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Author&pk={author_1.pk}&action_type=deassociate&relation_type=m2m"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_author_if_not_creator(
        self, client_editor, delete_url, author_1, passage_1, author_passage_1, regular
    ):
        assert author_1 in passage_1.authors.all()
        author_passage_1.user = regular
        author_passage_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert author_1 in passage_1.authors.all()
