from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-medium-create", args=(passage_1.pk,))


@pytest.fixture
def delete_url(passage_1, medium_1):
    return reverse(
        "web:passage-medium-delete",
        args=(passage_1.pk, medium_1.pk),
    )


@pytest.fixture
def medium_payload(language_french):
    return {
        "title": "New medium content.",
        "url": "https://example.org",
        "type": "image",
    }


class TestPassageMediumAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, medium_payload):
        response = client.post(create_url, medium_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageMediumEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_medium(
        self, client_editor, create_url, medium_payload, passage_1
    ):
        from meleager.models import Medium

        assert passage_1.images.count() == 0
        response = client_editor.post(create_url, medium_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_medium = Medium.objects.get(title=medium_payload["title"])
        assert response.url == (
            "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/"
            f"#medium-{created_medium.pk}"
        )
        assert created_medium in passage_1.images.all()

    def test_create_medium_generate_versions(
        self, client_editor, create_url, medium_payload, passage_1, editor
    ):
        from meleager.models import Medium

        response = client_editor.post(create_url, medium_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        created_medium = Medium.objects.get(title=medium_payload["title"])
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Medium&pk={created_medium.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_medium(self, client_editor, delete_url, medium_1, passage_1):
        assert medium_1 in passage_1.images.all()
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#media"
        assert medium_1 not in passage_1.images.all()

    def test_delete_medium_generate_versions(
        self, client_editor, delete_url, medium_1, passage_1, editor
    ):
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Medium&pk={medium_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_medium_if_not_creator(
        self, client_editor, delete_url, medium_1, passage_1, regular
    ):
        assert medium_1 in passage_1.images.all()
        medium_1.creator = regular
        medium_1.save()

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert medium_1 in passage_1.images.all()
