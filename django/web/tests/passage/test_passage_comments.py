from http import HTTPStatus

import pytest
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def create_url(passage_1):
    return reverse("web:passage-comment-create", args=(passage_1.pk,))


@pytest.fixture
def update_url(passage_1, comment_1):
    return reverse("web:passage-comment-update", args=(passage_1.pk, comment_1.pk))


@pytest.fixture
def delete_url(passage_1, comment_1):
    return reverse("web:passage-comment-delete", args=(passage_1.pk, comment_1.pk))


@pytest.fixture
def comment_payload(language_french):
    return {"description": "Comment desc FR", "language": language_french.code}


class TestPassageCommentAnonymous:
    def test_cannot_access_create_page(self, client, create_url):
        response = client.get(create_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page(self, client, create_url, comment_payload):
        response = client.post(create_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_update_page(self, client, update_url):
        response = client.get(update_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_update_page(self, client, update_url, comment_payload):
        response = client.post(update_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_access_delete_page(self, client, delete_url, comment_1):
        response = client.get(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_delete_page(self, client, delete_url, comment_1):
        response = client.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestPassageCommentEditor:
    def test_can_access_create_page(self, client_editor, create_url):
        response = client_editor.get(create_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_update_page(self, client_editor, update_url, comment_1):
        response = client_editor.get(update_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_access_delete_page(self, client_editor, delete_url, comment_1):
        response = client_editor.get(delete_url)
        assert response.status_code == HTTPStatus.OK

    def test_can_create_comment(
        self,
        client_editor,
        create_url,
        comment_payload,
        passage_1,
        comment_1,
        language_french,
    ):
        from meleager.models import Comment

        assert passage_1.comments.count() == 0
        response = client_editor.post(create_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_comment = Comment.objects.get(
            descriptions__description=comment_payload["description"]
        )
        assert (
            response.url
            == f"/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#comment-{created_comment.pk}"
        )
        assert created_comment in passage_1.comments.all()

    def test_create_comment_generate_versions(
        self, client_editor, create_url, comment_payload, passage_1, editor
    ):
        from meleager.models import Comment

        response = client_editor.post(create_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        created_comment = Comment.objects.get(
            descriptions__description=comment_payload["description"]
        )
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Comment&pk={created_comment.pk}&action_type=add&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_update_comment(
        self, client_editor, update_url, comment_payload, passage_1, comment_1
    ):
        passage_1.comments.add(comment_1)
        assert comment_1.descriptions.first().description == "Comment FR"
        response = client_editor.post(update_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url
            == f"/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#comment-{comment_1.pk}"
        )
        assert comment_1.descriptions.first().description == "Comment desc FR"
        assert comment_1 in passage_1.comments.all()

    def test_update_comment_generate_versions(
        self, client_editor, update_url, comment_payload, passage_1, comment_1, editor
    ):
        passage_1.comments.add(comment_1)
        response = client_editor.post(update_url, comment_payload)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Comment&pk={comment_1.pk}&action_type=modify&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_can_delete_comment(self, client_editor, delete_url, passage_1, comment_1):
        passage_1.comments.add(comment_1)
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        assert (
            response.url == "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/#comments"
        )
        assert comment_1 not in passage_1.comments.all()

    def test_delete_comment_generate_versions(
        self, client_editor, delete_url, passage_1, comment_1, editor
    ):
        passage_1.comments.add(comment_1)
        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FOUND
        versions = Version.objects.get_for_object(passage_1)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Comment&pk={comment_1.pk}&action_type=delete&relation_type=fk"
        )
        assert versions[0].revision.user == editor

    def test_cannot_delete_comment_if_not_creator(
        self, client_editor, delete_url, passage_1, comment_1, regular
    ):
        comment_1.creator = regular
        comment_1.save()
        passage_1.comments.add(comment_1)

        response = client_editor.post(delete_url)
        assert response.status_code == HTTPStatus.FORBIDDEN
        assert comment_1 in passage_1.comments.all()
