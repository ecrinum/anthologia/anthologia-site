from http import HTTPStatus

import pytest
import responses
from django.urls import reverse
from reversion.models import Version


@pytest.fixture
def list_url():
    return reverse("web:keyword-list")


@pytest.fixture
def detail_url(keyword_1):
    return reverse("web:keyword-detail", args=(keyword_1.pk,))


@pytest.fixture
def create_url_wikidata():
    return reverse("web:keyword-create-wikidata")


@pytest.fixture
def keyword_payload(keyword_category_1):
    def keyword_data(wiki_id):
        """Return a dict payload with a Wikidata URL (parameter)"""
        return {
            "category": keyword_category_1.pk,
            "alternative_urn_url_auto": f"https://www.wikidata.org/wiki/{wiki_id}",
        }

    return keyword_data


@pytest.mark.django_db
class TestKeywordAnonymous:
    def test_can_access_keywords_page(self, client, list_url, keyword_1):
        response = client.get(list_url)
        assert response.status_code == HTTPStatus.OK
        assert "Keywords" in str(response.content)
        print(response.content)
        assert keyword_1.names.first().name in str(response.content)

    def test_can_access_keyword_page(self, client, detail_url, keyword_1):
        response = client.get(detail_url)
        assert response.status_code == HTTPStatus.OK
        assert keyword_1.names.first().name in str(response.content)

    def test_cannot_access_create_page_wikidata(self, client, create_url_wikidata):
        response = client.get(create_url_wikidata)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")

    def test_cannot_submit_create_page_wikidata(
        self, client, create_url_wikidata, keyword_payload
    ):
        response = client.post(create_url_wikidata, keyword_payload("Q913"))
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/auth/login/?next=/")


@pytest.mark.django_db
class TestKeywordEditor:
    def test_can_access_create_page_wikidata(self, client_editor, create_url_wikidata):
        response = client_editor.get(create_url_wikidata)
        assert response.status_code == HTTPStatus.OK

    @responses.activate
    def test_can_create_keyword_from_wikidata(
        self,
        client_editor,
        create_url_wikidata,
        keyword_payload,
        languages,
        language_english,
        language_french,
        wikidata_json,
    ):
        from meleager.models import Keyword, URN

        assert Keyword.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q913.json",
            json=wikidata_json("Q913"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, keyword_payload("Q913"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/keywords/")
        assert Keyword.objects.count() == 1

        urn = URN.objects.get(urn="https://www.wikidata.org/wiki/Q913")
        assert urn.source == "wikidata"

        keyword_socrates = Keyword.objects.get(
            names__name="Socrates", names__language=language_english
        )
        assert urn in keyword_socrates.alternative_urns.all()
        assert Keyword.objects.get(
            names__name="Socrate", names__language=language_french
        )

    @responses.activate
    def test_can_create_keyword_from_wikidata_with_next_url(
        self,
        client_editor,
        create_url_wikidata,
        keyword_payload,
        languages,
        language_english,
        language_french,
        wikidata_json,
        book_1,
        passage_1,
    ):
        from meleager.models import Keyword

        assert Keyword.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q913.json",
            json=wikidata_json("Q913"),
            status=HTTPStatus.OK,
        )
        payload = keyword_payload("Q913")
        passage_url = reverse(
            "web:passage-detail",
            args=(book_1.number, passage_1.fragment, passage_1.sub_fragment),
        )
        payload["next_url"] = f"{passage_url}#passage-keyword-create"
        response = client_editor.post(create_url_wikidata, payload)

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith(
            "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/?keyword_created_pk="
        )
        assert response.url.endswith("#passage-keyword-create")
        assert Keyword.objects.count() == 1

    @responses.activate
    def test_cannot_create_duplicate_keyword_from_wikidata(
        self,
        client_editor,
        create_url_wikidata,
        keyword_payload,
        languages,
        language_english,
        language_french,
        wikidata_json,
    ):
        from meleager.models import Keyword, URN

        assert Keyword.objects.count() == 0

        # This first request should create a Keyword (setup step).
        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q913.json",
            json=wikidata_json("Q913"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, keyword_payload("Q913"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/keywords/")
        assert Keyword.objects.count() == 1

        # We post the same request again, should not create any
        # duplicate or modification.
        response = client_editor.post(create_url_wikidata, keyword_payload("Q913"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/keywords/")
        assert Keyword.objects.count() == 1

        urn = URN.objects.get(urn="https://www.wikidata.org/wiki/Q913")
        assert urn.source == "wikidata"

        keyword_socrates = Keyword.objects.get(
            names__name="Socrates", names__language=language_english
        )
        assert urn in keyword_socrates.alternative_urns.all()
        assert Keyword.objects.get(
            names__name="Socrate", names__language=language_french
        )

    @responses.activate
    def test_can_create_keyword_from_wikidata_generate_version(
        self,
        client_editor,
        create_url_wikidata,
        keyword_payload,
        languages,
        language_english,
        language_french,
        wikidata_json,
    ):
        from meleager.models import Keyword

        assert Keyword.objects.count() == 0

        responses.add(
            responses.GET,
            "https://www.wikidata.org/wiki/Special:EntityData/Q913.json",
            json=wikidata_json("Q913"),
            status=HTTPStatus.OK,
        )
        response = client_editor.post(create_url_wikidata, keyword_payload("Q913"))

        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith("/keywords/")
        assert Keyword.objects.count() == 1

        keyword = Keyword.objects.get(
            names__name="Socrates", names__language=language_english
        )
        versions = Version.objects.get_for_object(keyword)
        assert (
            versions[0].revision.get_comment()
            == f"app_label=meleager&model=Keyword&pk={keyword.pk}&action_type=create&relation_type="
        )
