from django import forms
from django.utils.translation import gettext as _

from meleager.forms.fields import PreferredLanguageField

from .fields import AlternativeURNField


class CityCreateWikidataURLForm(forms.Form):
    alternative_urn_url_auto = AlternativeURNField(
        required=True,
        label=_("Reference URL"),
        help_text=_(
            "URL of the city on the reference authority, "
            "for instance: https://www.wikidata.org/wiki/Q23482"
        ),
    )


class CityCreateNamesForm(forms.Form):
    alternative_urn_url = forms.URLField(required=False)
    alternative_urn_source = forms.CharField(required=False)
    name = forms.CharField(required=True)
    name_lang = PreferredLanguageField()
