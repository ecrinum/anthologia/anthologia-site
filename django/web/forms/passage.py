from django import forms
from django.forms import ModelChoiceField, ChoiceField
from django.utils.translation import gettext as _

from meleager.forms.passage_m2m import RelatedForm
from meleager.models import Manuscript, Passage, PassageInternalReference, Scholium


class PassageForm(forms.ModelForm):
    fragment = forms.CharField(disabled=True)
    sub_fragment = forms.CharField(disabled=True)

    class Meta:
        model = Passage
        fields = ["fragment", "sub_fragment"]
        exclude = ["urn", "images", "manuscripts", "keywords"]


class AddScholiumForm(forms.ModelForm):
    book = forms.CharField(disabled=True, required=False)
    fragment = forms.CharField(disabled=True, required=False)
    sub_fragment = forms.CharField(disabled=True, required=False)

    class Meta:
        model = Scholium
        fields = ["book", "fragment", "sub_fragment", "number"]


class AddManuscriptForm(forms.ModelForm):
    title = forms.CharField(required=True, label=_("Credits"))

    class Meta:
        model = Manuscript
        fields = ["title", "url"]


class InternalReferenceField(ModelChoiceField):
    """Custom field to display names with translations, neatly organized

    It creates the `choices` from the queryset keyword argument.
    """

    def __init__(self, queryset, **kwargs):
        super().__init__(queryset, **kwargs)
        self.choices = [("", "")] + queryset.get_passage_choices()


class InternalReferenceForm(forms.Form):

    reference_type = forms.ChoiceField(
        label=_("Reference type"),
        required=True,
        choices=PassageInternalReference.REFERENCE_TYPE,
    )
    
    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop("queryset")
        super().__init__(*args, **kwargs)
        self.fields['to_passage'] = InternalReferenceField(queryset=queryset)
