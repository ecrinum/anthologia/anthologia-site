class Turbo extends Stimulus.Controller {
    static get targets() {
        return ['wrapper']
    }

    static values = {
        url: String,
        autocomplete: String,
        fetch: Boolean,
        search: String
    }

    fetchValueChanged() {
        function checkStatus(response) {
            if (response.status >= 200 && response.status < 300) {
                return response
            } else {
                const error = new Error(response.statusText)
                error.response = response
                throw error
            }
        }

        if (this.fetchValue) {
            fetch(this.urlValue + this.searchValue)
                .then(checkStatus)
                .then((response) => response.text())
                .then((html) => {
                    const parser = new DOMParser()
                    const doc = parser.parseFromString(html, 'text/html')
                    const form = doc.querySelector('form')
                    this.wrapperTarget.innerHTML = form.outerHTML
                    if (this.hasAutocompleteValue) {
                        this._autocomplete()
                    }
                })
                .catch((error) => {
                    console.error('Failed to fetch page: ', error)
                    if (error.response.status === 403) {
                        this.wrapperTarget.innerHTML = `
                            <h1 style="color:red;padding:2rem;">
                                ${error.response.statusText}
                                (${error.response.status})
                            </h1>
                        `
                    }
                })
        }
    }

    _autocomplete() {
        accessibleAutocomplete.enhanceSelectElement({
            selectElement: document.querySelector(`#${this.autocompleteValue}`),
            showAllValues: true,
            dropdownArrow: (config) =>
                '<svg class="' +
                config.className +
                '" style="top: 14px;" viewBox="0 0 512 512" ><path d="M256,298.3L256,298.3L256,298.3l174.2-167.2c4.3-4.2,11.4-4.1,15.8,0.2l30.6,29.9c4.4,4.3,4.5,11.3,0.2,15.5L264.1,380.9  c-2.2,2.2-5.2,3.2-8.1,3c-3,0.1-5.9-0.9-8.1-3L35.2,176.7c-4.3-4.2-4.2-11.2,0.2-15.5L66,131.3c4.4-4.3,11.5-4.4,15.8-0.2L256,298.3  z"/></svg>',
        })
    }
}
