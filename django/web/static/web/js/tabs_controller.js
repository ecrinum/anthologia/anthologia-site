class Tabs extends Stimulus.Controller {
    static get targets() {
        return ['link', 'content']
    }

    _unselectAllTabs() {
        this.linkTargets.forEach((tabLink) => {
            tabLink.parentElement.classList.remove('selected')
            tabLink.setAttribute('aria-selected', false)
        })
    }

    _hideAll() {
        this.contentTargets.forEach((tabContent) => {
            tabContent.setAttribute('hidden', '')
        })
    }

    tabFocus = 0

    keyboardA11Y(event) {
        /* Inspired from:
        https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/Tab_Role */
        const ARROW_RIGHT = 39
        const ARROW_LEFT = 37
        if (event.keyCode === ARROW_RIGHT || event.keyCode === ARROW_LEFT) {
            this.linkTargets[this.tabFocus].setAttribute('tabindex', -1)
            if (event.keyCode === ARROW_RIGHT) {
                this.tabFocus++
                // If we're at the end, go to the start
                if (this.tabFocus >= this.linkTargets.length) {
                    this.tabFocus = 0
                }
            } else if (event.keyCode === ARROW_LEFT) {
                this.tabFocus--
                // If we're at the start, move to the end
                if (this.tabFocus < 0) {
                    this.tabFocus = this.linkTargets.length - 1
                }
            }

            this.linkTargets[this.tabFocus].setAttribute('tabindex', 0)
            this.linkTargets[this.tabFocus].focus()
        }
    }

    switch(event) {
        event.preventDefault()
        this._unselectAllTabs()
        this._hideAll()
        const clickedTab = event.target
        clickedTab.parentElement.classList.add('selected')
        clickedTab.setAttribute('aria-selected', true)
        const targetContent = this.contentTarget.parentElement.querySelector(
            clickedTab.hash
        )
        targetContent.removeAttribute('hidden')
    }
}
