from django.utils.translation import gettext as _

from meleager.forms.text import TextForm
from meleager.models import Text
from meleager.views.mixins import PassageRelatedMixin, ScholiumRelatedMixin
from meleager.views.related import (
    RelatedCreateView,
    RelatedDeleteView,
    RelatedUpdateView,
)


class TextMixin:
    model = Text
    related_name = "text"
    success_url_anchor_name = "texts"
    success_url_add_pk_to_anchor = False


class TextCreateMixin(TextMixin):
    template_name = "web/text/text_create.html"
    form_class = TextForm
    action = "create"


class TextWarningMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        text = context["text"]
        has_alignments = (
            text.meleager_alignment_text_1.count()
            or text.meleager_alignment_text_2.count()
        )
        if has_alignments:
            context["warnings"] = [
                _("Warning: an alignment exists for that text!"),
            ]
        return context


class TextUpdateMixin(TextMixin, TextWarningMixin):
    template_name = "web/text/text_update.html"
    form_class = TextForm
    action = "update"


class TextDeleteMixin(TextMixin, TextWarningMixin):
    template_name = "web/text/text_delete.html"


class PassageTextCreate(TextCreateMixin, PassageRelatedMixin, RelatedCreateView):
    pass


class PassageTextUpdate(TextUpdateMixin, PassageRelatedMixin, RelatedUpdateView):
    pass


class PassageTextDelete(TextDeleteMixin, PassageRelatedMixin, RelatedDeleteView):
    pass

class ScholiumTextCreate(TextCreateMixin, ScholiumRelatedMixin, RelatedCreateView):
    pass


class ScholiumTextUpdate(TextUpdateMixin, ScholiumRelatedMixin, RelatedUpdateView):
    pass


class ScholiumTextDelete(TextDeleteMixin, ScholiumRelatedMixin, RelatedDeleteView):
    pass
