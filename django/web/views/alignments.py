from urllib.parse import urlencode

import reversion
from django.core.exceptions import PermissionDenied
from django.views import generic

from meleager.forms.alignment import AlignmentCreateForm, AlignmentUpdateForm
from meleager.models import Alignment, Text
from meleager.views.mixins import MlgrPermissionsMixin, PassageRelatedMixin


class AlignmentMixin:
    model = Alignment
    related_name = "alignment"
    success_url_anchor_name = "alignments"
    success_url_add_pk_to_anchor = False


class PassageAlignmentCreate(
    AlignmentMixin, MlgrPermissionsMixin, PassageRelatedMixin, generic.CreateView
):
    form_class = AlignmentCreateForm
    template_name = "web/alignment/alignment_create.html"
    action = "create"

    def get_context_data(self, **kwargs):
        context = super(PassageAlignmentCreate, self).get_context_data(**kwargs)

        # You need at least 2 texts to make an alignment.
        passage_texts = Text.objects.filter(passages=self.get_base_object().pk)
        if passage_texts.count() < 2:
            raise PermissionDenied

        context["form"].fields["text_1"].initial = passage_texts[0]
        context["form"].fields["text_1"].queryset = passage_texts
        context["form"].fields["text_1"].required = True
        context["form"].fields["text_1"].help_text = ""
        context["form"].fields["text_2"].queryset = passage_texts
        context["form"].fields["text_2"].required = True
        context["form"].fields["text_2"].help_text = ""
        context["passage_texts"] = passage_texts
        return context

    def form_valid(self, form):
        # Add the current user as a creator.
        obj = form.save(commit=False)
        obj.creator = self.request.user
        obj.save()

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": obj.__class__.__name__,
                "pk": obj.pk,
                "action_type": "add",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        return (
            self.get_base_object().get_absolute_url()
            + f"#{self.success_url_anchor_name}"
        )


class PassageAlignmentUpdate(
    AlignmentMixin, MlgrPermissionsMixin, PassageRelatedMixin, generic.UpdateView
):
    form_class = AlignmentUpdateForm
    template_name = "web/alignment/alignment_update.html"
    action = "update"

    def get_context_data(self, **kwargs):
        context = super(PassageAlignmentUpdate, self).get_context_data(**kwargs)
        context["text_1"] = self.object.text_1
        context["text_2"] = self.object.text_2
        context["alignment"] = self.object
        return context

    def form_valid(self, form):
        current_object = self.get_object()

        # Check for permissions before saving the object
        if not self.check_user_perm_for_obj(self.request.user, current_object):
            raise PermissionDenied

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": current_object.__class__.__name__,
                "pk": current_object.pk,
                "action_type": "modify",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        return (
            self.get_base_object().get_absolute_url()
            + f"#{self.success_url_anchor_name}"
        )


class PassageAlignmentDelete(
    AlignmentMixin, MlgrPermissionsMixin, PassageRelatedMixin, generic.DeleteView
):
    template_name = "web/alignment/alignment_delete.html"
    action = "delete"

    def form_valid(self, form):
        current_object = self.get_object()

        # Check for permissions before deleting the object
        if not self.check_user_perm_for_obj(self.request.user, current_object):
            raise PermissionDenied

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": current_object.__class__.__name__,
                "pk": current_object.pk,
                "action_type": "delete",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        return f"{self.get_base_object().get_absolute_url()}#{self.success_url_anchor_name}"
