from collections import defaultdict
from itertools import chain

from django.shortcuts import get_object_or_404
from django.views import generic
from django.urls import reverse
from django.utils.translation import gettext as _
from reversion.models import Version

from meleager.models import Passage


class PassageDetail(generic.DetailView):
    model = Passage
    template_name = "web/passage/passage.html"
    context_object_name = "passage"

    def get_queryset(self):
        return Passage.objects.select_related("book", "creator").prefetch_related(
            "authors",
            "authors__names",
            "comments",
            "comments__descriptions",
        )

    def get_object(self):
        return get_object_or_404(
            self.get_queryset(),
            book__number=self.kwargs.get("book"),
            fragment=self.kwargs.get("fragment"),
            sub_fragment=self.kwargs.get("sub_fragment", ""),
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        passage = self.object
        user = self.request.user

        context["page_title"] = str(passage)
        context["texts"] = list(
            chain(
                passage.texts.filter(language__code="grc")
                .select_related("edition", "language")
                .prefetch_related("meleager_alignment_text_1__text_2"),
                passage.texts.exclude(language__code="grc")
                .select_related("edition", "language")
                .prefetch_related("meleager_alignment_text_1__text_2"),
            )
        )

        # CREATE PERMISSION
        context["can_create"] = user.is_authenticated and user.has_perm(
            "can_create_mlgr_content"
        )

        # KEYWORDS AND CATEGORIES ARE NESTED IN THE VIEW
        kw_with_perms = passage.get_m2m_objects_with_perms(
            self.request.user, "keywords", "keyword"
        )

        kw_categories = defaultdict(list)
        for kw in kw_with_perms:
            cat = kw["object"].category
            kw_categories[cat].append(kw)

        context["keywords_categories"] = dict(kw_categories)

        # AUTHORS
        authors = passage.get_m2m_objects_with_perms(
            self.request.user, "authors", "author"
        )
        for elem in authors:
            elem["preferred_names"] = (
                elem["object"]
                .names.filter(language__preferred=True)
                .values("name", "language__code")
            )
        context["authors"] = authors

        # CITIES
        cities = passage.get_m2m_objects_with_perms(self.request.user, "cities", "city")
        for elem in cities:
            elem["preferred_names"] = (
                elem["object"]
                .names.filter(language__preferred=True)
                .values("name", "language__code")
            )
        context["cities"] = cities
        context["url_to_all_modifications"] = reverse(
            "web:passage-history",
            args=(
                passage.book.number,
                passage.fragment,
                passage.sub_fragment,
            ),
        )
        context["internal_references"] = (
            passage.internal_references.through.objects.filter(from_passage=passage)
            .select_related("from_passage__book", "to_passage__book")
            .order_by("from_passage__book__number", "from_passage__fragment")
        )
        context["versions"] = Version.objects.get_for_object(passage)[:5]
        return context


class PassageHistory(generic.DetailView):
    template_name = "web/history/list.html"
    context_object_name = "passage"
    model = Passage

    def get_object(self):
        obj = get_object_or_404(
            self.get_queryset(),
            book__number=self.kwargs.get("book"),
            fragment=self.kwargs.get("fragment"),
            sub_fragment=self.kwargs.get("sub_fragment", ""),
        )
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        passage = self.get_object()
        context["passage"] = passage
        context["page_title"] = _("Modifications for %(passage)s") % {
            "passage": passage
        }
        context["versions"] = Version.objects.get_for_object(passage)
        return context
