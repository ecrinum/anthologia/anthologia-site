from urllib.parse import urlencode

import reversion
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect

from meleager.models import Passage, PassageInternalReference
from meleager.views.mixins import PassageRelatedMixin
from meleager.views.related import RelatedCreateView, RelatedDeleteView

from web.forms.passage import InternalReferenceForm


class InternalReferenceCreate(PassageRelatedMixin, RelatedCreateView):
    model = PassageInternalReference
    form_class = InternalReferenceForm
    related_name = "internal_reference"
    template_name = "web/references/internal_reference_create.html"
    submit_view = "web:passage-internal-reference-create"
    success_url_anchor_name = "internal-references"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.pop("instance")
        passage = self.get_base_object()
        existing_references = passage.internal_references.values_list("pk", flat=True)
        excluded_references = [passage.pk] + list(existing_references)
        kwargs["queryset"] = Passage.objects.exclude(pk__in=excluded_references)
        return kwargs

    def form_valid(self, form):
        from_passage = self.get_base_object()
        to_passage_pk = form["to_passage"].value()
        reference_type = form["reference_type"].value()
        to_passage = Passage.objects.get(pk=to_passage_pk)

        # We create both symmetrical relations.
        self.model.objects.get_or_create(
            from_passage=from_passage,
            to_passage=to_passage,
            reference_type=reference_type,
            creator=self.request.user,
        )
        self.model.objects.get_or_create(
            from_passage=to_passage,
            to_passage=from_passage,
            reference_type=reference_type,
            creator=self.request.user,
        )
        # Then we update the comment for the current reversion and
        # create a revision for each of the from/to passages.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": "Passage",
                "pk_from": from_passage.pk,
                "pk_to": to_passage_pk,
                "action_type": "add",
                "relation_type": "self",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(from_passage)
        reversion.add_to_revision(to_passage)

        return redirect(self.get_absolute_url())

    def get_absolute_url(self):
        anchor = self.success_url_anchor_name
        return f"{self.get_base_object().get_absolute_url()}#{anchor}"


class InternalReferenceDelete(PassageRelatedMixin, RelatedDeleteView):
    model = PassageInternalReference
    template_name = "web/references/internal_reference_delete.html"
    related_name = "internal_reference"
    submit_view = "web:passage-internal-reference-delete"
    success_url_anchor_name = "internal-references"

    def form_valid(self, request, *args, **kwargs):
        current_object = self.get_object()
        if not self.check_user_perm_for_obj(self.request.user, current_object):
            raise PermissionDenied

        from_passage = current_object.from_passage
        to_passage = current_object.to_passage

        # We update the comment for the current reversion and
        # create a revision for each of the from/to passages.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": "Passage",
                "pk_from": from_passage.pk,
                "pk_to": to_passage.pk,
                "action_type": "delete",
                "relation_type": "self",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(from_passage)
        reversion.add_to_revision(to_passage)

        # Then we remove the current internal reference and the symmetrical one.
        current_object.delete()
        symmetrical_object = self.model.objects.get(
            from_passage=to_passage, to_passage=from_passage
        )
        symmetrical_object.delete()

        return redirect(self.get_absolute_url())

    def get_absolute_url(self):
        anchor = self.success_url_anchor_name
        return f"{self.get_base_object().get_absolute_url()}#{anchor}"
