from meleager.models import Manuscript
from meleager.views.mixins import PassageRelatedMixin, ScholiumRelatedMixin
from meleager.views.related import RelatedCreateView, RelatedDeleteView
from ..forms.passage import AddManuscriptForm


class ManuscriptMixin:
    model = Manuscript
    related_name = "manuscript"
    success_url_anchor_name = "manuscripts"
    success_url_add_pk_to_anchor = False


class ManuscriptCreateMixin(ManuscriptMixin):
    template_name = "web/manuscript/manuscript_create.html"
    form_class = AddManuscriptForm
    action = "create"


class ManuscriptDeleteMixin(ManuscriptMixin):
    template_name = "web/manuscript/manuscript_delete.html"


class PassageManuscriptCreate(
    ManuscriptCreateMixin, PassageRelatedMixin, RelatedCreateView
):
    pass


class PassageManuscriptDelete(
    ManuscriptDeleteMixin, PassageRelatedMixin, RelatedDeleteView
):
    pass


class ScholiumManuscriptCreate(
    ManuscriptCreateMixin, ScholiumRelatedMixin, RelatedCreateView
):
    pass


class ScholiumManuscriptDelete(
    ManuscriptDeleteMixin, ScholiumRelatedMixin, RelatedDeleteView
):
    pass
