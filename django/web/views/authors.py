from django.shortcuts import reverse
from django.utils.translation import gettext as _
from django.views import generic

from meleager.forms.passage_m2m import AuthorForm
from meleager.models import Author, AuthorPassage
from meleager.models.name import sort_names_by_languages
from meleager.views.m2m import M2MDeleteView, M2MFormView
from meleager.views.mixins import PassageRelatedMixin


class AuthorDetail(generic.DetailView):
    model = Author
    template_name = "web/author/detail.html"

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.prefetch_related(
            "names", "names__language", "passages", "passages__book"
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        author = context["object"]
        context["author_names"] = sort_names_by_languages(
            author.names.values("name", "language__code", "language__code2"),
            key="language__code2",
        )
        context["page_title"] = context["author_names"][0]["name"]

        context["passages"] = [
            {
                "pk": elem["pk"],
                "short_title": "{}.{}{}".format(
                    elem["book__number"], elem["fragment"], elem["sub_fragment"]
                ),
                "url": reverse(
                    "web:passage-detail",
                    args=(elem["book__number"], elem["fragment"], elem["sub_fragment"]),
                ),
            }
            for elem in author.passages.values(
                "pk", "book__number", "fragment", "sub_fragment"
            )
        ]

        return context


class AuthorList(generic.ListView):
    model = Author
    template_name = "web/author/list.html"
    # context_object_name = "authors"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = _("Authors")
        context["authors"] = [
            {"pk": pk, "names": names}
            for pk, names in self.get_queryset().get_name_choices()
        ]
        return context


class PassageAuthorCreate(PassageRelatedMixin, M2MFormView):
    model = AuthorPassage
    form_class = AuthorForm
    template_name = "web/author/author_create.html"
    related_name = "author"
    action = "create"
    success_url_anchor_name = "authors"


class PassageAuthorDelete(PassageRelatedMixin, M2MDeleteView):
    model = AuthorPassage
    template_name = "web/author/author_delete.html"
    related_name = "author"
    success_url_anchor_name = "authors"
