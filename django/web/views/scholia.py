from collections import defaultdict
from urllib.parse import urlencode

from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import generic

import reversion
from reversion.models import Version

from meleager.models import Passage, Scholium
from meleager.views.mixins import AddContentPermReqMixin, PassageRelatedMixin

from ..forms.passage import AddScholiumForm


class ScholiumDetail(generic.DetailView):
    model = Scholium
    template_name = "web/scholium/detail.html"
    context_object_name = "scholium"

    def get_object(self):
        book = self.kwargs.get("book")
        fragment = self.kwargs.get("fragment")
        sub_fragment = self.kwargs.get("sub_fragment")
        number = self.kwargs.get("number")

        return get_object_or_404(
            Scholium.objects.select_related("passage__book", "creator"),
            passage__book__number=book,
            passage__fragment=fragment,
            passage__sub_fragment=sub_fragment,
            number=number,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        scholium = self.object

        # KEYWORDS AND CATEGORIES ARE NESTED IN THE VIEW
        kw_with_perms = scholium.get_m2m_objects_with_perms(
            self.request.user, "keywords", "keyword"
        )

        kw_categories = defaultdict(list)
        for kw in kw_with_perms:
            cat = kw["object"].category
            kw_categories[cat].append(kw)

        # CITIES
        cities = scholium.get_m2m_objects_with_perms(
            self.request.user, "cities", "city"
        )
        for elem in cities:
            elem["preferred_names"] = (
                elem["object"]
                .names.filter(language__preferred=True)
                .values("name", "language__code")
            )

        passage = scholium.passage
        context.update(
            {
                "book": passage.book,
                "passage": passage,
                "cities": cities,
                "page_title": str(scholium),
                "keywords_categories": dict(kw_categories),
                "url_to_all_modifications": reverse(
                    "web:scholium-history",
                    args=(
                        passage.book.number,
                        passage.fragment,
                        passage.sub_fragment,
                        scholium.number,
                    ),
                ),
                "versions": Version.objects.get_for_object(scholium)[:5],
            }
        )
        return context


class ScholiumCreate(
    AddContentPermReqMixin,
    PassageRelatedMixin,
    generic.detail.SingleObjectMixin,
    generic.FormView,
):
    model = Passage
    form_class = AddScholiumForm
    template_name = "web/scholium/scholium_create.html"
    context_object_name = "passage"
    pk_url_kwarg = "passage_pk"
    action = "create"
    related_name = "scholium"

    @property
    def submit_view(self):
        return f"web:{self.related_name}-{self.action}"

    def get_initial(self, **kwargs):
        passage = self.get_object()
        max_number = max(
            (scholium.number for scholium in passage.scholia.all()), default=0
        )
        return {
            "number": max_number + 1,
            "book": passage.book.number,
            "fragment": passage.fragment,
            "sub_fragment": passage.sub_fragment,
        }

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super().get_context_data(**kwargs)
        context["page_title"] = _("Create Scholium for %(passage)s") % {
            "passage": self.object
        }
        context["create_url"] = reverse(
            "web:scholium-create", kwargs={"passage_pk": self.kwargs.get("passage_pk")}
        )
        return context

    def form_valid(self, form):
        passage = self.get_object()
        number = form.cleaned_data.get("number")
        scholium, created = Scholium.objects.get_or_create(
            number=number,
            passage_id=passage.pk,
            defaults={"creator": self.request.user},
        )
        if created:
            comment = urlencode(
                {
                    "app_label": "meleager",
                    "model": scholium.__class__.__name__,
                    "pk": scholium.pk,
                    "action_type": "create",
                    "relation_type": "fk",
                }
            )
            reversion.set_comment(comment)
            reversion.add_to_revision(passage)
        return redirect(scholium.get_absolute_url())


class ScholiumHistory(generic.DetailView):
    model = Scholium
    template_name = "web/history/list.html"
    context_object_name = "scholium"

    def get_object(self):
        book = self.kwargs.get("book")
        fragment = self.kwargs.get("fragment")
        sub_fragment = self.kwargs.get("sub_fragment")
        number = self.kwargs.get("number")

        return get_object_or_404(
            Scholium,
            passage__book__number=book,
            passage__fragment=fragment,
            passage__sub_fragment=sub_fragment,
            number=number,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        scholium = self.get_object()
        context["scholium"] = scholium
        context["page_title"] = _("Modifications for %(scholium)s") % {
            "scholium": scholium
        }
        context["versions"] = Version.objects.get_for_object(scholium)
        return context
