from urllib.parse import urlencode

import reversion
from django.shortcuts import redirect

from meleager.forms.description import DescriptionForm
from meleager.models import Comment, Description
from meleager.views.mixins import (
    PassageRelatedMixin,
    ScholiumRelatedMixin,
)
from meleager.views.related import (
    RelatedCreateView,
    RelatedDeleteView,
    RelatedUpdateView,
)


class CommentMixin:
    model = Description
    form_class = DescriptionForm
    related_name = "comment"


class CommentCreateView(CommentMixin, RelatedCreateView):
    template_name = "web/description/description_create.html"
    action = "create"

    def form_valid(self, form):
        description = form.save()
        comment = Comment.objects.create(creator=self.request.user)
        comment.descriptions.add(description)
        self.get_base_object().comments.add(comment)
        reversion_comment = urlencode(
            {
                "app_label": "meleager",
                "model": comment.__class__.__name__,
                "pk": comment.pk,
                "action_type": "add",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(reversion_comment)
        return redirect(self.get_success_url(comment))

    def get_success_url(self, obj):
        return (
            f"{self.get_base_object().get_absolute_url()}#{self.related_name}-{obj.pk}"
        )


class CommentUpdateView(CommentMixin, RelatedUpdateView):
    template_name = "web/description/description_update.html"
    action = "update"

    def get_comment(self):
        return Comment.objects.get(pk=self.kwargs.get("comment_pk"))

    def get_object(self):
        return self.get_comment().descriptions.first()

    def form_valid(self, form):
        # We pass a Comment current object because it is a Description by default.
        return super().form_valid(form, current_object=self.get_comment())


class CommentDeleteView(RelatedDeleteView):
    model = Comment
    template_name = "web/comment/comment_delete.html"
    action = "delete"
    related_name = "comment"

    def get_success_url(self):
        """Redirect to the base object"""
        return f"{self.get_base_object().get_absolute_url()}#{self.success_url_anchor_name}"

    def form_valid(self, form):
        # Make sure we delete the descriptions too
        self.get_object().descriptions.all().delete()
        return super().form_valid(form)


class PassageCommentCreate(PassageRelatedMixin, CommentCreateView):
    pass


class PassageCommentUpdate(PassageRelatedMixin, CommentUpdateView):
    pass


class PassageCommentDelete(PassageRelatedMixin, CommentDeleteView):
    success_url_anchor_name = "comments"


class ScholiumCommentCreate(ScholiumRelatedMixin, CommentCreateView):
    pass


class ScholiumCommentUpdate(ScholiumRelatedMixin, CommentUpdateView):
    pass


class ScholiumCommentDelete(ScholiumRelatedMixin, CommentDeleteView):
    success_url_anchor_name = "comments"
