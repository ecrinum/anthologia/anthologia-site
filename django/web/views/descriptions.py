from meleager.forms.description import DescriptionForm
from meleager.models import Description
from meleager.views.mixins import PassageRelatedMixin, ScholiumRelatedMixin
from meleager.views.related import (
    RelatedCreateView,
    RelatedDeleteView,
    RelatedUpdateView,
)


class DescriptionMixin:
    model = Description
    related_name = "description"


class DescriptionCreateMixin(DescriptionMixin):
    template_name = "web/description/description_create.html"
    form_class = DescriptionForm


class DescriptionUpdateMixin(DescriptionMixin):
    template_name = "web/description/description_update.html"
    form_class = DescriptionForm


class DescriptionDeleteMixin(DescriptionMixin):
    template_name = "web/description/description_delete.html"


class PassageDescriptionCreate(
    PassageRelatedMixin, DescriptionCreateMixin, RelatedCreateView
):
    pass


class PassageDescriptionUpdate(
    PassageRelatedMixin, DescriptionUpdateMixin, RelatedUpdateView
):
    pass


class PassageDescriptionDelete(
    PassageRelatedMixin, DescriptionDeleteMixin, RelatedDeleteView
):
    success_url_anchor_name = "descriptions"


class ScholiumDescriptionCreate(
    ScholiumRelatedMixin, DescriptionCreateMixin, RelatedCreateView
):
    pass


class ScholiumDescriptionUpdate(
    ScholiumRelatedMixin, DescriptionUpdateMixin, RelatedUpdateView
):
    pass


class ScholiumDescriptionDelete(
    ScholiumRelatedMixin, DescriptionDeleteMixin, RelatedDeleteView
):
    success_url_anchor_name = "descriptions"
