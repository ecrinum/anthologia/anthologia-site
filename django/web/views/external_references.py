from meleager.forms.passage_m2m import ExternalReferenceForm
from meleager.models import ExternalReference
from meleager.views.mixins import PassageRelatedMixin
from meleager.views.related import RelatedCreateView, RelatedDeleteView


class ExternalReferenceCreate(PassageRelatedMixin, RelatedCreateView):
    model = ExternalReference
    form_class = ExternalReferenceForm
    related_name = "external_reference"
    template_name = "web/references/external_reference_create.html"
    submit_view = "web:passage-external-reference-create"
    success_url_anchor_name = "external-reference"


class ExternalReferenceDelete(PassageRelatedMixin, RelatedDeleteView):
    model = ExternalReference
    template_name = "web/references/external_reference_delete.html"
    related_name = "external_reference"
    submit_view = "web:passage-external-reference-delete"
    success_url_anchor_name = "external-references"
