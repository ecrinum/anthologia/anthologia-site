from meleager.forms.passage_m2m import MediumForm
from meleager.models import Medium
from meleager.views.mixins import PassageRelatedMixin
from meleager.views.related import RelatedCreateView, RelatedDeleteView


class MediumCreate(PassageRelatedMixin, RelatedCreateView):
    model = Medium
    form_class = MediumForm
    related_name = "image"
    template_name = "web/media/medium_create.html"
    submit_view = "web:passage-medium-create"
    success_url_anchor_name = "medium"


class MediumDelete(PassageRelatedMixin, RelatedDeleteView):
    model = Medium
    template_name = "web/media/medium_delete.html"
    related_name = "image"
    submit_view = "web:passage-medium-delete"
    success_url_anchor_name = "media"
    pk_url_kwarg = "medium_pk"
