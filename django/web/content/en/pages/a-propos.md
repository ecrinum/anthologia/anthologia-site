---
title: About
---

“Pour une édition numérique collaborative de l’*Anthologie grecque*” is a project of the [Canada Research Chair on Digital Textualities](http://ecrituresnumeriques.ca/en/) (directed by Marcello Vitali-Rosati) in collaboration with Elsa Bouchard and Christian Raschle. The platform [Anthologia graeca](https://anthologiagraeca.org/) aims to gather as much information and data as possible on the *Greek Anthology* and to produce new ones, thus continuing the anthological enterprise. The platform includes, among other things, different versions of the Greek text, various translations into different languages, notes, commentaries, information on the authors, images of the codex (the *palatinus graecus* 23), keywords, places cited, etc. 

Everyone is welcome to collaborate, whether you are just curious or a specialist!

## The *Greek Anthology*

The word "anthology" literally means "gathering, wreath of flowers" and is equivalent to the original Latin word "florilegium". The *Greek Anthology* collects the "flowers" (epigrams) of Greek literature. The epigrams consist of small versified pieces, which had for initial vocation to be inscribed in order to accompany a statue, an ex-voto, an epitaph,... The genre develops and becomes more diverse during the Alexandrian period particularly, encompassing, among others, amorous, satirical or bacchic topics. 

What we call *Greek Anthology* corresponds to a compilation of Greek epigrammatic poetry from the classical to the Byzantine period, that is, more than 4,000 pieces, by 325 different authors; the result of sixteen centuries of literature. As a result from multiple compilations and reconfigurations, the *Anthology* has a complex history. By definition, an anthological collection is characterized by the use of several sources and the continuous enrichment of a corpus. 

The name "Greek Anthology" refers to the reunion of the *codex Palatinus 23* and the *appendix planudea*. 
The *Codex palatinus 23* (940 AD) was retrieved in 1606 by Claude Saumaise and is known as the *Palatine Anthology*. The manuscript travelled through many hands throughout Europe before arriving at the Palatine Library in Heidelberg where it is currently held. However, during a journey between the *Palatine* and *Vatican* libraries, it was torn in two at page 614, between books XIII and XIV. Pages 615 to 709 are still kept at the National Library of France.
The *Appendix Planudea* includes the epigrams absent from the Palatine manuscript but present in the *Anthology* of Planudes (1301), a Byzantine scholar who lived in the 13<sup>th</sup> and 14<sup>th</sup> centuries. 
The *Greek Anthology* is divided into 16 books by theme. The first book, for example, includes Christian epigrams, the fifth gathers amorous and erotic epigrams, the seventh, epitaphs or funerary epigrams. The sixteenth book corresponds to the *appendix planudea*. 

These anthologies are based on an anthology compiled by Constantine Cephalas around 900 AD, itself composed from the Garland of Meleager (1<sup>th</sup> century BC), Philippe (1<sup>th</sup> century AD), Diogenianus (2<sup>th</sup> century AD) or the *Cycle* of Agathias (later, in the 6<sup>th</sup> century AD). 

Despite its inherent fragmentation, the *Anthology* presents itself as a corpus rich in intertextuality: through the repetition of iconic themes, topoi emerge that are found in our current culture and that mark a timelessness of the *Anthology*. These poems have had a major influence on literature from the Renaissance to the present day.  

## Description and objectives of the project 

Since 2014, a large team has been contributing to the project of a collaborative and digital edition of the *Greek Anthology*. Its purpose is to offer the most *complete* edition possible for each of the epigrams of the *Anthology* - even though, by its anthological nature, the project refuses any closure. 
The editing [platform](https://anthologiagraeca.org/) dedicated to the project is built on a relational database, structured on the notion of entity (1 entity = 1 epigram). Each entity is associated with several types of editable information including :  
- the image of the manuscript *Pal. gr. 23* corresponding to the epigram (thanks to the iiif protocol) ;  
- transcriptions ;  
- translations ;  
- alignments of the translations;  
- image and transcription of the *scholia* ;  
- internal and external references ;  
- keywords (structured according to the recommendations of the Linked Open Data) ;  
- commentaries.   

These data are retrievable through an [API](https://anthologiagraeca.org/api/), making all the data produced in the project accessible in JSON format.

This edition is not meant to compete with the numerous existing critical editions of the *Greek Anthology* but rather to suggest an alternative philological approach, allowing the anthological project initiated by the first compilers to be continued.
The very essence of *Anthology* is the linking of its texts. In fact, to give an account of the anthological imagination does not mean to make a classical critical and genetic edition in which it would be a question of establishing a truth of the text, but rather to underline the innumerable connections between texts and other cultural objects. Our database is organized around a text entity, since a text can have different versions and translations. The idea of the "truth" of the text - the basis of the critical or genealogical approach - is not our objective. On the contrary, we seek to bring out the pluralities of perceptions of the textual material - for it is indeed this plurality, at the origin of the collective imagination woven around the *Greek Anthology*, which constitutes, for us, the essence of the text. Our database must allow us to complete this anthological philosophy, by underlining the intertextual and intermediary connections that have been woven through time. To account for this heterogeneous richness, we have chosen an open, flexible editorial form that allows for dynamic interaction between the scholarly and the more amateur community.
By renewing the classical philological approach, we have been led to shift the boundaries between scholarly and amateur practices, both in terms of production and dissemination of editorial content.

## New projects 

Other projects were created in parallel to the [main project](https://anthologiagraeca.org/). 

### The [POP](http://pop.anthologiegrecque.org/#/) (Plateforme Ouverte des Parcours d'imaginaires). 

It is a visualization of an earlier version of the platform, then called *Anthologia*. It gathers in thematic reading paths some of the epigrams of the *Anthology*. 
Unfortunately, the website is currently down. 

### *Intelligence Artificielle Littéraire (IAL) : for an algorithmic model of variation in the Greek Anthology*. 

This is a pilot project whose objective is to clarify the definition of the concept of variation by using algorithms capable of finding examples of it within the *Anthology*. The algorithms are not used for heuristic purposes (we know what they should find) but rather for to support literary theory. Indeed, the parameters used for the algorithms - if they return what we are looking for - will allow us to approach a formal definition of variation.

The data can be found [here](https://gitlab.huma-num.fr/ecrinum/anthalgo).

### HTR Models for the CP gr. 23 

We developed a recognition model for Ancient Greek based on the *Heidelbergensis Palatinus graecus* 23 using eScriptorium. Three models have been produced and are available on Zenodo: 

- [HTR Model Palatinus graecus 23 (Meleagre-NFD)](https://zenodo.org/records/10932742) -- accuracy : 90,85% ; 
- [HTR Model Palatinus graecus 23 (Meleagre-NFC)](https://zenodo.org/records/10932711)-- accuracy : 91,00% ;  
- [HTR Model Palatinus graecus 23 (Meleagre-NFD-finetuned)](https://zenodo.org/records/10932751) -- accuracy : 91,05%. 

The repository containing the data and transcription information is public and can be found [here](https://gitlab.huma-num.fr/ecrinum/anthologia/htr_cpgr23).
<!--
- [Anthological Navigations : The Greek Anthology in the Era of Digital Classics](http://navigations.digitaltextualities.ca/): The CRCDT is organizing, on October 27<sup>th</sup>, 28<sup>th</sup>, and 29<sup>th</sup>, a three-day workshop dedicated to the *Greek Anthology*'s actuality. The aim of this meeting is to call upon the community of *Digital Classicists* to reflect on the scientific issues related to the dissemination of the heritage of ancient Greek languages and literatures and, more generally, on the methodological and epistemological impacts of the Digital Humanities within the framework of Hellenistic studies, starting with the case study of the *Greek Anthology*. While focusing on the anthological corpus, this event aims to bridge the gap between the terms "digital" and "classicists", to provoke a meeting between researchers, professors and programmers with diverse profiles and sometimes distant interests, so that a perennial discussion can be created and new research potentialities can emerge.-->