---
title: Index
--- 

## Le projet

« Pour une édition numérique collaborative de l’*Anthologie grecque* » est un projet de la [Chaire de recherche du Canada sur les écritures numériques](http://ecrituresnumeriques.ca) (dirigée par Marcello Vitali-Rosati) en collaboration avec Elsa Bouchard et Christian Raschle. 

## Cette documentation

Sur ce site vous trouverez :   

1. [Des informations générales sur le projet](../a-propos/)
2. [Une liste et une description des différentes ressources du projet](../ressources/)
3. [Un guide pour l’utilisation de la plateforme et de l’API](../documentation-technique/)
4. [Les évènements, conférence, et articles liés au projet](../communications/)
5. [Des informations sur notre équipe et sur nos partenaires, ainsi que nos contacts](../equipe-et-partenaires/)
