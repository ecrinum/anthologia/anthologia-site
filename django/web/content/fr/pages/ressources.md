---
title: Ressources
--- 

## Le manuscrit 

La majorité du *Codex Palatinus 23* est disponible [ici](https://digi.ub.uni-heidelberg.de/diglit/cpgraec23/).
Les pages 615 à 709 du manuscrit se trouvent [ici](https://gallica.bnf.fr/ark:/12148/btv1b8470199g/) ([notice](https://archivesetmanuscrits.bnf.fr/ark:/12148/cc24643g)).  

## La transcription 

La transcription est disponible sur [Perseus](http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3atext%3a2008.01.0472). Il s'agit de l'édition de W. R. Paton, Londres, William Heinemann Ltd. 1915-1927 (5 volumes). 

## Les éditions 

Les éditions utilisées sur la plateforme sont les suivantes (les références suivent la norme APA 7^th^ ed,) : 

### éditions savantes 

- Paton edition :   
	- Paton, W. R. (1916). The Greek Anthology: Vol. I (Books 1-6). Harvard University Press.  
	- Paton, W. R. (1917). The Greek Anthology: Vol. II (Books 7-8). Harvard University Press.  
	- Paton, W. R. (1917). The Greek Anthology: Vol. III (Book 9). Harvard University Press.  
	- Paton, W. R. (1918). The Greek Anthology: Vol. IV (Books 10-12). Harvard University Press.  
	- Paton, W. R. (1918). The Greek Anthology: Vol. V (Books 13-16). Harvard University Press.  

- Waltz edition :   
	- Waltz, P. (1928a). Anthologie grecque. Anthologie Palatine: Vol. I (livres I-IV). Les Belles Lettres.  
	- Waltz, P. (1928b). Anthologie grecque. Anthologie Palatine: Vol. II (livre V). Les Belles Lettres. 
	- Waltz, P. (1931). Anthologie grecque. Anthologie Palatine: Vol. III (livre VI). Les Belles Lettres.  
	- Waltz, P. (1938). Anthologie grecque. Anthologie Palatine: Vol. IV (livre VII, épigr. 1-363) (P. Camelot, A. Dain, E. Des Places, & A. M. Desrousseaux, Trad.). Les Belles Lettres.
	- Waltz, P. (1960a). Anthologie grecque. Anthologie Palatine: Vol. V (livre VII, épigr. 364-748) (E. Des Places, M. Dumitrescu, H. Le Maitre, & G. Soury, Trad.). Les Belles Lettres.
	- Waltz, P. (1960b). Anthologie grecque. Anthologie Palatine: Vol. VI (livre VIII). Les Belles Lettres.  
	- Waltz, P. (1957). Anthologie grecque. Anthologie Palatine: Vol. VII (livre IX, épigr. 1-358) (G. Soury, Trad.). Les Belles Lettres.  
	- Waltz, P., & Soury, G. (1974). Anthologie grecque. Anthologie Palatine: Vol. VIII (livre IX, épigr. 359-827). Les Belles Lettres.
	- Irigoin, J., & Maltomini, F. (2011). Anthologie grecque. Anthologie Palatine: Vol. IX (livre X) (P. Laurens, Trad.). Les Belles Lettres.  
	- Aubreton, R. (1972). Anthologie grecque. Anthologie Palatine: Vol. X (livre XI). Les Belles Lettres.  
	- Aubreton, R., Buffière, F., & Irigoin, J. (1994). Anthologie grecque. Anthologie Palatine: Vol. XI (livre XII). Les Belles Lettres.  
	- Buffière, F. (1970). Anthologie grecque. Anthologie Palatine: Vol. XII (livres XIII-XV). Les Belles Lettres.   
	- Aubreton, R. (1980). Anthologie grecque. Anthologie de Planude: Vol. XIII. Les Belles Lettres.  
	
- Edition Scholia :  
	- Stadtmüller, H. (1894). Anthologia graeca epigrammatum Palatina cum Planudea: Vol. I (Palatinae Libr. I-VI (Planudeae Libr. V-VII)). B. G. Teubneri. http://archive.org/details/bub_gb_LfPfAAAAMAAJ  
	- Stadtmüller, H. (1899). Anthologia graeca epigrammatum Palatina cum Planudea: Vol. II (Palatinae Libr. VII (Planudeae Libr. III)). B. G. Teubneri. http://archive.org/details/bub_gb_K__fAAAAMAAJ  
	- Stadtmüller, H. (1906). Anthologia graeca epigrammatum Palatina cum Planudea: Vol. III (Palatinae Libr. IX epp. 1-563 (Planudeae Libr. I)). B. G. Teubneri. https://digi.ub.uni-heidelberg.de/diglit/stadtmueller1906bd3_1  

- Beckby : 
	- Beckby, H. (1957). Anthologia graeca (Griechisch-Deutsch): Vol. I (Buch I-VI) (Ernst Heimeran).
	- Beckby, H. (1957). Anthologia graeca (Griechisch-Deutsch): Vol. II (Buch VII-VIII) (Ernst Heimeran).
	- Beckby, H. (1958). Anthologia graeca (Griechisch-Deutsch): Vol. III (Buch IX-XI) (Ernst Heimeran).
	- Beckby, H. (1958). Anthologia graeca (Griechisch-Deutsch): Vol. IV (Buch XII-XVI) (Ernst Heimeran).

- M. Yourcenar : 
	- Yourcenar, M. (1979). La Couronne et la Lyre. Anthologie de la poésie grecque ancienne. Gallimard.

- Fernando Pessoa : 
	- Pessoa, F. (2001). Poesias Coligidas. Editora Nova Aguilar.  

- Conca-Marzi(-Zanetto) : 
	- Conca, F., Marzi, M., & Zanetto, G. (2005). Antologia Palatina. Libri I - VII. (Unione Tipografico-Editrice Torinese, Vol. 1).   
	- Conca, F., & Marzi, M. (2009). Antologia Palatina. Libri VIII - XI (Unione Tipografico-Editrice Torinese, Vol. 2).  
	- Conca, F., & Marzi, M. (2011). Antologia Palatina. Libri XII - XVI (Unione Tipografico-Editrice Torinese, Vol. 3).

- Pontani : 
	- Pontani, F. M. (1978). Antologia Palatina: Vol. I (libri 1-6). Einaudi.  
	- Pontani, F. M. (1979). Antologia Palatina: Vol. II (libri 7-8). Einaudi.  
	- Pontani, F. M. (1980). Antologia Palatina: Vol. III (libri 9-11). Einaudi.  
	- Pontani, F. M. (1981). Antologia Palatina: Vol. IV (libri 12-16). Einaudi.  

- Anthologia Graeca cum versione Latina Hugonis Grotii : 
	- Grotius, H. (1795). Anthologia Graeca cum versione Latina Hugonis Grotii: Vol. I (J. de Bosch, Éd.). B. Wild & J. Altheer. http://hdl.handle.net/2027/hvd.hnjwzq
	- Grotius, H. (1797). Anthologia Graeca cum versione Latina Hugonis Grotii: Vol. II (J. de Bosch, Éd.). B. Wild & J. Altheer. http://hdl.handle.net/2027/hvd.hnjwzr
	- Grotius, H. (1798). Anthologia Graeca cum versione Latina Hugonis Grotii: Vol. III (J. de Bosch, Éd.). B. Wild & J. Altheer. http://hdl.handle.net/2027/hvd.hnjwzs
	- Bosch, J. de, Grotius, H., & Saumaise, C. (1810). Observationes et Notae in Anthologiam Graecam, quibus accedunt Cl. Salmasii Notae Ineditae: Vol. IV. B. Wild & J. Altheer. http://hdl.handle.net/2027/hvd.hnjwzt
	- Bosch, J. de, Grotius, H., & Lennep, D. J. van. (1822). Observationum et Notarum in Anthologiam Graecam, volumen alterum, quod et indices continet: Vol. V. B. Wild & J. Altheer. ttp://hdl.handle.net/2027/hvd.hnjwzu

	
### Éditions amateures	
	
- Marcello Vitali-Rosati, chercheur principal  
- Elsa Bouchard, chercheuse principale  
- Édition Mathilde Verstraete, coordinatrice scientifique du projet
- Luiz Capelo, auxiliaire de recherche   
- Ulysse Bouchard, auxiliaire de recherche 
- Paton modified : adaptation de l'édition de Paton
- Paton modified by Maxime : adaptation de l'édition de Paton
- Paton modified by Ulysse Bouchard : adaptation de l'édition de Paton
- Cagnazzi : Traductions réalisées par les étudiant·e·s du [Liceo Cagnazzi (Bari)](https://liceocagnazzi.edu.it/) sous la supervision de leur professeure, Annalisa di Vincenzo
- Liceo Majorana : Traductions réalisées par les étudiant·e·s du [Liceo Majorana](https://www.liceomajorana.edu.it/it/) 
- Edition Matilda Chapman : Stagiaire Mitacs
- Edition Jie Han : Stagiaire Mitacs 

<!--
### QESTIONS/TODO

as-tu le nom de la prof du lycée Majorana ?
qu'est-ce que "Snyder??"
qu'est-ce que "pk 18" ?
il y a "R.P. André Bremond" mais sais pas comment retrouver l'oeuvre dont c'est issu...
qui est Sebastiana Nervegna ?
4 aubreton 
Réduire les 3 tomes UTET -->


## Bibliographie 

Le projet a sa propre [librairie Zotero](https://www.zotero.org/groups/2484902/anthologiepalatine/library). Si vous désirez ajouter des références, vous pouvez contacter [la CRCEN](mailto:crc.ecrituresnumeriques@gmail.com).

## Visualisations

- [POP](http://pop.anthologiegrecque.org/) : La Plateforme Ouverte des Parcours d'imaginaires (crée à partir de l'[ancienne version](http://anthologia.ecrituresnumeriques.ca) de la plateforme) permet de visualiser les épigrammes dans les versions grecques et les traductions (en français, anglais et italien), leurs métadonnées (auteur, date, lieu et mots-clefs) sous la forme de parcours de lecture thématiques. 

## Outils

- [Twitter Bot GreekAnthology](https://twitter.com/greekAnthology)
