from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

urlpatterns = [
    path("api/", include("meleager_api.urls")),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("", include("web.urls")),
    path("admin/", admin.site.urls),
    path("select2/", include("django_select2.urls")),
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
    path("auth/", include("meleager_user.auth_urls")),
    path("user/", include("meleager_user.user_urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
