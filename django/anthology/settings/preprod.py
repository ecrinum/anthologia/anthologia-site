from . import *  # noqa

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

SECRET_KEY = "qv!wbd*(@1wx&et0djf#2w_rjww_c$$((u0z@4uck1faf=3!mq"

ALLOWED_HOSTS = ["ovh.anthologiagraeca.org"]

sentry_sdk.init(
    dsn="https://7be080984d5b415b88b2628181dbac2a@o1222326.ingest.sentry.io/6366129",
    integrations=[DjangoIntegration()],
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=0.1,
    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,
    environment="preprod",
)

STATIC_ROOT="/home/ubuntu/anthologia-site/static_root"