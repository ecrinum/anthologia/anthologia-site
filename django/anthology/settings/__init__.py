import os
from pathlib import Path
from django.utils.translation import gettext_lazy as _

SECRET_KEY = "Dummy Key"

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = Path(__file__).resolve(strict=True).parents[2]


# Application definition

INSTALLED_APPS = [
    "web.apps.WebConfig",
    "meleager.apps.MeleagerConfig",
    "meleager_import.apps.MeleagerImportConfig",
    "meleager_user.apps.MeleagerUserConfig",
    "meleager_api.apps.MeleagerApiConfig",
    "reversion",
    "reversion_compare",
    "django_extensions",
    "django_select2",
    "rest_framework",
    "django_filters",
    "graphene_django",
    "django.contrib.gis",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "corsheaders",
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "anthology.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "web.context_processors.preferred_languages",
            ]
        },
    }
]

WSGI_APPLICATION = "anthology.wsgi.application"


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "anthology",
        "USER": "anthology_django",
        "PASSWORD": "password",
        "HOST": "localhost",
        "PORT": 5432,
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]
LOGOUT_REDIRECT_URL = "/"

CORS_ALLOW_ALL_ORIGINS = True
CORS_URLS_REGEX = r"^/api/.*$"
CORS_ALLOW_METHODS = [
    "GET",
    "OPTIONS",
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/


LANGUAGES = [
    ("la", _("Latin")),
    ("en", _("English")),
    ("fr", _("French")),
    ("it", _("Italian")),
    ("pt", _("Portuguese")),
    ("cmn", _("Mandarin Chinese")),
]
LOCALE_PATHS = [
    os.path.join(BASE_DIR, "locale"),
]

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

AUTH_USER_MODEL = "meleager_user.User"

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/tmp/django_cache",
    },
    "select2": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "/tmp/django_cache",
        # "BACKEND": "django_redis.cache.RedisCache",
        # "LOCATION": "redis://127.0.0.1:6379/2",
        # "OPTIONS": {
        #     "CLIENT_CLASS": "django_redis.client.DefaultClient",
        # }
    },
}

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "DEFAULT_FILTER_BACKENDS": ["django_filters.rest_framework.DjangoFilterBackend"],
    "PAGE_SIZE": 1,
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "WARNING",
    },
    "loggers": {
        "meleager": {
            "handlers": ["console"],
            "level": os.getenv("DJANGO_LOG_LEVEL", "INFO"),
            "propagate": False,
        },
    },
}

# See issue #254 - Django does not know when SSL offloading is done on a reverse proxy
# https://stackoverflow.com/questions/62047354/build-absolute-uri-with-https-behind-reverse-proxy

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
STATIC_URL = "/static/"
STATIC_ROOT = "/static"

# The number of validation level permissions in this application.
# Changing this settings require rerunning "manage.py makemigrations" and "manage.py migrate"
# (and optionally a re-importation of the data)
MAX_VALIDATION_LEVEL = 2

AP_API_IMPORT_DIR = "/import"
# Configure Graphene schema
GRAPHENE = {"SCHEMA": "meleager.schema.schema"}

LOGIN_URL = "/auth/login/"
LOGIN_REDIRECT_URL = "/user/profile/"

# django-reversion-compare
ADD_REVERSION_ADMIN = True
# optional settings:
REVERSION_COMPARE_FOREIGN_OBJECTS_AS_ID = False
REVERSION_COMPARE_IGNORE_NOT_REGISTERED = False

MARKDOWNIFY = {
    "default": {
        "WHITELIST_TAGS": [
            "p",
            "h1",
            "h2",
            "strong",
            "em",
            "ul",
            "ol",
            "li",
        ]
    },
}
