import debug_toolbar
from django.urls import include, path

from .urls import urlpatterns

urlpatterns.append(path("__debug__/", include(debug_toolbar.urls)))
