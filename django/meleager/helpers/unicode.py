import unicodedata


def strip_accents(s):
    # Source: https://stackoverflow.com/a/518232
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )
