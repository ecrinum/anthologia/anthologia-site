from .meleager import MlgrTestCase
from meleager.models import Passage, Text


class PassageModelTest(MlgrTestCase):
    def test_creating_text_updates_passage_timestamp(self):
        passage = Passage.objects.first()
        updated_at_1 = passage.updated_at
        text = Text.objects.create(
            text="new text", language_id="eng", unique_id=99999999
        )
        passage.texts.add(text)
        passage.refresh_from_db()
        self.assertGreater(passage.updated_at, updated_at_1)

    def test_associating_text_updates_passage_timestamp(self):
        passage = Passage.objects.first()
        updated_at_1 = passage.updated_at
        text = Text.objects.create(
            text="new text", language_id="eng", unique_id=9999999
        )
        text.passages.add(passage)
        passage.refresh_from_db()
        self.assertGreater(passage.updated_at, updated_at_1)

    def test_prev_next_urls(self):
        p = Passage.objects.create(book=self.passage.book, fragment="666")
        p2 = Passage.objects.create(book=self.passage.book, fragment="666", sub_fragment="a")

        self.assertEqual(p.get_next_url(), p2.get_absolute_url())
        self.assertEqual(p2.get_previous_url(), p.get_absolute_url())

        # This can only work with the good fixtures... (passage2 in make_fixtures.py)
        self.assertEqual(self.passage.get_next_url(), "/passages/urn:cts:greekLit:tlg7000.tlg001.ag:42.69abc/")
