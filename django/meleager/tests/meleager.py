from django.test import TestCase

from meleager.models import City, Keyword, Passage, Scholium
from meleager_user.models import User


class MlgrTestCase(TestCase):
    """Test case for Meleager development

    It loads the test fixtures, and creates useful class instances:
    - passage: PK 1
    - scholium: PK 1
    - editor: user with editor privileges
    - editor_admin: user with editor admin privileges
    - keyword: keyword PK 1

    Allows to:
    - login as editor / editor_admin

    Test methods:
    - assertRedirectToLoginPage: checks the response has a redirect to the login page
      checks for the `next` parameter in the keyword arguments
    """

    fixtures = ["test_data.json"]

    @classmethod
    def setUpTestData(cls):
        cls.passage = Passage.objects.get(pk=1)
        cls.scholium = Scholium.objects.get(pk=1)
        cls.editor = User.objects.get(username="editor")
        cls.editor_admin = User.objects.get(username="editor_admin")
        cls.keyword = Keyword.objects.get(pk=1)
        cls.city = City.objects.get(pk=1)

    def _mlgr_login(self, value):
        self.client.logout()
        self.client.login(username=value, password=value)

    def login_editor(self):
        self._mlgr_login("editor")

    def login_editor_admin(self):
        self._mlgr_login("editor_admin")

    def assertRedirectToLoginPage(self, response, *args, **kwargs):
        next = kwargs.get("next")

        # Assumes the login page is there
        login_url = "/auth/login/"

        if next:
            login_url = f"{login_url}?next={next}"
            del kwargs["next"]

        self.assertRedirects(response, login_url, **kwargs)
