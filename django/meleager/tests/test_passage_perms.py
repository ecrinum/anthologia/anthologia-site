from django.test import TestCase
from meleager.models import AuthorPassage, CityPassage, KeywordPassage, Passage
from meleager_user.models import User


class PassageModelTest(TestCase):
    fixtures = ["test_data.json"]

    def test_admin_editor_unlink_any(self):
        """ Make sure the admin editor user can remove any relation """
        passage = Passage.objects.first()
        admin_editor = User.objects.get(username="editor_admin")
        related = (
            (AuthorPassage, "authors", "author"),
            (CityPassage, "cities", "city"),
            (KeywordPassage, "keywords", "keyword"),
        )

        for (m2m_model, passage_field, m2m_field) in related:
            perms = passage.get_m2m_objects_with_perms(
                admin_editor, passage_field, m2m_field
            )
            self.assertTrue(perms[0]["can_be_unlinked"])

    def test_unlink_permission_passage(self):
        """Make sure a user can only delete objects they own

        In the fixtures, all relations are owned by the "editor" user.
        """
        passage = Passage.objects.first()
        user = User.objects.create_user(username="forbidden")

        related = (
            (AuthorPassage, "authors", "author"),
            (CityPassage, "cities", "city"),
            (KeywordPassage, "keywords", "keyword"),
        )

        for (m2m_model, passage_field, m2m_field) in related:
            perms = passage.get_m2m_objects_with_perms(user, passage_field, m2m_field)
            self.assertFalse(perms[0]["can_be_unlinked"])

            filter_dict = {"passage_id": passage.pk, m2m_field: perms[0]["object"].pk}
            m2m_model.objects.filter(**filter_dict).update(user_id=user.pk)

            perms = passage.get_m2m_objects_with_perms(user, passage_field, m2m_field)
            self.assertTrue(perms[0]["can_be_unlinked"])
