import json
from pathlib import Path
from unittest.mock import Mock, patch

from django.test import TestCase
from meleager.helpers.wikidata import get_wiki_id_from_url, get_wiki_names_from_url


def mock_json(wiki_id):
    """Creates a Mock object that has JSON output from Wikidata.

    Takes the wiki id as argument, and reads from the "data" folder relative to this test file.
    """

    mock = Mock()

    with open(Path(__file__).parent / "data" / f"{wiki_id}.json") as fp:
        data = json.load(fp)

    mock.json = lambda *a, **k: data
    mock.url = f"https://www.wikidata.org/wiki/{wiki_id}"
    return mock


class WikidataTest(TestCase):
    fixtures = ["test_data.json"]

    def test_get_kw_id_from_url(self):
        """Test the wiki entity ID extraction from the URL"""
        url = "https://www.wikidata.org/wiki/Q913"
        self.assertEqual(get_wiki_id_from_url(url), "Q913")

        url = "https://google.com"
        self.assertIsNone(get_wiki_id_from_url(url))

        url = "toto"
        self.assertIsNone(get_wiki_id_from_url(url))

    def test_get_kw_names_invalid_url(self):
        """The function returns an empty dict for invalid/incorrect URLs"""
        self.assertEqual(get_wiki_names_from_url("http://google.com"), {})
        self.assertEqual(get_wiki_names_from_url("toto"), {})

    def test_get_kw_names_from_wiki_url(self):
        """Make sure languages are retrieved from Wikidata"""
        mock = mock_json("Q747483")
        with patch("requests.get", return_value=mock):
            names = get_wiki_names_from_url("https://www.wikidata.org/wiki/Q747483")
            self.assertEqual(names["eng"], "erotic literature")

    def test_get_kw_names_from_wiki_url_with_grc(self):
        """Make sure we take the "grc" version from the P1559 claim in Wikidata"""
        mock = mock_json("Q913")
        with patch("requests.get", return_value=mock):
            names = get_wiki_names_from_url("https://www.wikidata.org/wiki/Q913")
            self.assertEqual(names["grc"], "Σωκράτης")
