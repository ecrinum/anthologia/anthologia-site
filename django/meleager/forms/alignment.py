from django import forms
from django.utils.safestring import mark_safe

from meleager.models import Alignment


class AlignmentCreateForm(forms.ModelForm):
    class Meta:
        model = Alignment
        fields = ("text_1", "text_2", "alignment_data")
        widgets = {"alignment_data": forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(AlignmentCreateForm, self).__init__(*args, **kwargs)
        self.fields["alignment_data"].widget.attrs[
            "data-alignments-creation-target"
        ] = "data"
        self.fields["text_1"].widget.attrs["data-alignments-creation-target"] = "left"
        self.fields["text_1"].widget.attrs["data-action"] = mark_safe(
            "alignments-creation#display"
        )
        self.fields["text_2"].widget.attrs["data-action"] = mark_safe(
            "alignments-creation#display"
        )


class AlignmentUpdateForm(forms.ModelForm):
    class Meta:
        model = Alignment
        fields = ("text_1", "text_2", "alignment_data")
        widgets = {
            "text_1": forms.HiddenInput(),
            "text_2": forms.HiddenInput(),
            "alignment_data": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(AlignmentUpdateForm, self).__init__(*args, **kwargs)
        self.fields["alignment_data"].widget.attrs[
            "data-alignments-creation-target"
        ] = "data"
        self.fields["text_1"].widget.attrs["data-alignments-creation-target"] = "left"
        self.fields["text_1"].widget.attrs["data-action"] = mark_safe(
            "alignments-creation#display"
        )
        self.fields["text_2"].widget.attrs["data-action"] = mark_safe(
            "alignments-creation#display"
        )
