from iso639 import languages

# from django.db import models

Language = ((lang.part3, lang.name) for lang in languages if lang.part3)

# Language = models.TextChoices(
#     "Language",
#     {: (l.part3, l.name) for l in languages if l.part3},
# )
