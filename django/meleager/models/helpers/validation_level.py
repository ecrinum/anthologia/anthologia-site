from django.conf import settings
from django.db import models

ValidationLevel = models.IntegerChoices(
    "ValidationLevel",
    {
        "VALIDATION_LEVEL_{}".format(i): (
            i,
            "Validation level {}, see ValidationLevelName.validation_level = {} for the validation level name.".format(
                i, i
            ),
        )
        for i in range(settings.MAX_VALIDATION_LEVEL)
    },
)


ValidationLevelPermission = models.TextChoices(
    "ValidationLevelPermission",
    {
        "SET_VALIDATION_LEVEL_PERMISSION_{}".format(i): (
            "set_validation_level_{}".format(i),
            "The permission to set validation level {}, see ValidationLevelName.validation_level = {} for the validation level name.".format(
                i, i
            ),
        )
        for i in range(settings.MAX_VALIDATION_LEVEL)
    },
)
