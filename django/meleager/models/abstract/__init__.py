from .unique_id import AbstractUniqueIdentifier
from .descriptable import AbstractDescriptableResource
from .editable import AbstractEditableResource
from .validable import AbstractValidableResource
from .alternative_urns import AlternativeURNs

__all__ = [
    "AbstractUniqueIdentifier",
    "AbstractDescriptableResource",
    "AbstractEditableResource",
    "AbstractValidableResource",
    "AlternativeURNs",
]
