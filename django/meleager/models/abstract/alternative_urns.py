from django.db import models


class AlternativeURNs(models.Model):
    alternative_urns = models.ManyToManyField("meleager.URN")

    class Meta:
        abstract = True
