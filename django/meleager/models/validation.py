from django.db import models

from .helpers import ValidationLevel

# TODO : It could be good that the VALIDATION_LEVEL_0 is always the "UNVALIDATED" State and the
# VALIDATION_LEVEL_1 the "VALIDATED" State as sugested by Zach


class ValidationLevelName(models.Model):
    """
    Model that holds the association between the ValidationLevel index and theirs translations
    """

    validation_level = models.IntegerField(
        choices=ValidationLevel.choices,
        unique=True,
        help_text="The validation level integer associated with name",
    )

    # TODO : may someone add the help_text to document this field ? : I don't understand it
    rank = models.IntegerField()
    name = models.TextField(unique=True, help_text="The name of this validation level")
