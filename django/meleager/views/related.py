from urllib.parse import urlencode

import reversion
from django.core.exceptions import PermissionDenied
from django.views import generic

from .mixins import AddContentPermReqMixin, MlgrPermissionsMixin


class RelatedCreateView(AddContentPermReqMixin, generic.CreateView):
    """Generic view to create a related object"""

    action = "create"

    def form_valid(self, form):
        """Create the related object and add it to the base object

        The user is set to the current user making the request.
        """
        obj = form.save(commit=False)
        obj.creator = self.request.user
        obj.save()
        getattr(self.get_base_object(), self.base_field).add(obj)

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": obj.__class__.__name__,
                "pk": obj.pk,
                "action_type": "add",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        """Fancy redirection to the related object that was created"""
        anchor = self.success_url_anchor_name

        if (
            not hasattr(self, "success_url_add_pk_to_anchor")
            or self.success_url_add_pk_to_anchor is True
        ):
            anchor = f"{anchor}-{self.kwargs.get(self.pk_url_kwarg, self.object.pk)}"

        return f"{self.get_base_object().get_absolute_url()}#{anchor}"


class RelatedUpdateView(MlgrPermissionsMixin, generic.UpdateView):
    """Generic view to update a related object"""

    action = "update"

    def form_valid(self, form, current_object=None):
        """Check for permissions"""
        current_object = current_object or self.get_object()
        if not self.check_user_perm_for_obj(self.request.user, current_object):
            raise PermissionDenied

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": current_object.__class__.__name__,
                "pk": current_object.pk,
                "action_type": "modify",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        """Fancy redirection to the anchor/PK of the updated object"""
        anchor = self.success_url_anchor_name

        if (
            not hasattr(self, "success_url_add_pk_to_anchor")
            or self.success_url_add_pk_to_anchor is True
        ):
            anchor = f"{anchor}-{self.kwargs.get(self.pk_url_kwarg)}"

        return f"{self.get_base_object().get_absolute_url()}#{anchor}"


class RelatedDeleteView(MlgrPermissionsMixin, generic.DeleteView):
    """Generic view to delete a related object"""

    action = "delete"

    def form_valid(self, form):
        """Check for permissions before deleting the object"""
        current_object = self.get_object()
        if not self.check_user_perm_for_obj(self.request.user, current_object):
            raise PermissionDenied

        # Add a revision to the base object.
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": current_object.__class__.__name__,
                "pk": current_object.pk,
                "action_type": "delete",
                "relation_type": "fk",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        """Redirect to the base object"""
        return f"{self.get_base_object().get_absolute_url()}#{self.success_url_anchor_name}"
