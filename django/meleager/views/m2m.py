from urllib.parse import urlencode

import reversion
from django.core.exceptions import PermissionDenied
from django.views import generic

from .mixins import AddContentPermReqMixin, MlgrPermissionsMixin


class M2MFormView(AddContentPermReqMixin, generic.FormView):
    """Generic view for explicit management of related objects through explicit M2M models

    To be used with create/update views.
    """

    def form_valid(self, form):
        """Validates the form

        In our case, it adds the current user as the owner of the relation.
        The related object is not checked for anything, but it should (TODO).
        """
        args = {
            self.base_name: self.get_base_object(),
            self.related_name: form.cleaned_data[self.related_name],
            "user": self.request.user,
        }
        current_object = self.model(**args)
        current_object.save()

        # In case of a M2M, the current_object is AuthorPassage alike,
        # in that case the target_model will be Author.
        target_model = getattr(current_object, self.related_name)
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": target_model.__class__.__name__,
                "pk": target_model.pk,
                "action_type": "associate",
                "relation_type": "m2m",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_form_kwargs(self):
        """Extra arguments for the form class

        This is useful because we use custom fields that expect a queryset, which is dynamic
        (depends on the base object considered).

        For example, we want to exclude Authors that are already linked to a Passage in the form.
        """
        kwargs = super().get_form_kwargs()

        if hasattr(self.get_base_object(), self.base_field):
            related = getattr(self.get_base_object(), self.base_field).all()
            already_linked = related.values_list("pk", flat=True)
            target_model = related.model
            # Exclude the objects that are already linked to the base object
            kwargs["queryset"] = target_model.objects.exclude(pk__in=already_linked)

        return kwargs

    def get_success_url(self):
        """Redirect to the related object list."""
        anchor = self.success_url_anchor_name
        return f"{self.get_base_object().get_absolute_url()}#{anchor}"


class M2MDeleteView(MlgrPermissionsMixin, generic.DeleteView):
    """Generic view to delete a related object with an explicit M2M relationship"""

    action = "delete"

    def get_object(self):
        """Get the M2M object from the DB"""
        args = {
            self.fk_related: self.kwargs.get(self.pk_url_kwarg),
            self.fk_base: self.kwargs.get(self.base_pk_url_kwarg),
        }
        return self.get_queryset().get(**args)

    def get_context_data(self, **kwargs):
        """Add the related object to the context"""
        context = super().get_context_data(**kwargs)
        context[self.context_object_name] = getattr(
            self.get_object(), self.related_name
        )
        return context

    def form_valid(self, form):
        """Check for permissions before deleting the object"""
        current_object = self.get_object()

        if not self.check_user_perm_for_obj(self.request.user, current_object, field="user"):
            raise PermissionDenied

        # In case of a M2M, the current_object is AuthorPassage alike,
        # in that case the target_model will be Author.
        target_model = getattr(current_object, self.related_name)
        comment = urlencode(
            {
                "app_label": "meleager",
                "model": target_model.__class__.__name__,
                "pk": target_model.pk,
                "action_type": "deassociate",
                "relation_type": "m2m",
            }
        )
        reversion.set_comment(comment)
        reversion.add_to_revision(self.get_base_object())

        return super().form_valid(form)

    def get_success_url(self):
        """Redirect to the related object list."""
        anchor = self.success_url_anchor_name
        return f"{self.get_base_object().get_absolute_url()}#{anchor}"
