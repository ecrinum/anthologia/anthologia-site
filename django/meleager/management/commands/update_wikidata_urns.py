import requests
from django.core.management.base import BaseCommand

from meleager.helpers.wikidata import get_wiki_id_from_url


class Command(BaseCommand):
    help = "Update redirected wikidata URNs preserving relations."

    def handle(self, *args, **options):
        from meleager.models import URN

        self.stdout.write(self.style.SUCCESS("Command update wikidata URNs starting…"))

        deleted = 0
        modified = 0
        updated = 0

        for urn in URN.objects.filter(urn__contains="wikidata.org"):
            # TODO: deal with extra spaces in URLs.
            wiki_id = get_wiki_id_from_url(urn.urn.strip())
            if not wiki_id:
                self.stdout.write(
                    self.style.WARNING("wiki_id cannot be extracted from {urn.urn}")
                )
                continue

            wikidata_api_url = (
                f"https://www.wikidata.org/wiki/Special:EntityData/{wiki_id}.json"
            )
            try:
                resp = requests.get(wikidata_api_url)
                resp.raise_for_status()
            except requests.exceptions.HTTPError:
                self.stdout.write(
                    self.style.WARNING(f"{urn.urn} does not exist (anymore?)")
                )
                continue
            # Warning, wikidata redirections are not handled at the API level.
            # For instance https://www.wikidata.org/wiki/Special:EntityData/Q97620996.json
            # contains the wiki_id Q12877290 (the frontend does redirect though).
            data = resp.json()["entities"].get(wiki_id)
            if data is None:
                redirected_wiki_id = list(resp.json()["entities"].keys())[0]
                redirected_urn = f"https://www.wikidata.org/wiki/{redirected_wiki_id}"
                if URN.objects.filter(urn=redirected_urn).exists():
                    self.stdout.write(
                        self.style.NOTICE(
                            (
                                f"Merging https://www.wikidata.org/wiki/{wiki_id} "
                                f"and {redirected_urn}"
                            )
                        )
                    )
                    target_urn = URN.objects.get(urn=redirected_urn)
                    urn_relations = urn.get_relations()
                    target_urn_relations = target_urn.get_relations()
                    for kind, queryset in urn_relations.items():
                        if queryset.count() and sorted(
                            queryset.values_list("pk")
                        ) != sorted(target_urn_relations[kind].values_list("pk")):
                            for item in queryset:
                                self.stdout.write(
                                    self.style.NOTICE(
                                        (
                                            f"Adding {target_urn} to {item} "
                                            f"(type:{kind}) instead of {urn}"
                                        )
                                    )
                                )
                                item.alternative_urns.add(target_urn)
                    self.stdout.write(self.style.NOTICE((f"Deleting {urn}.")))
                    urn.delete()
                    deleted += 1

                else:
                    self.stdout.write(
                        self.style.NOTICE(
                            (
                                f"Redirecting from https://www.wikidata.org/wiki/{wiki_id} "
                                f"to {redirected_urn}"
                            )
                        )
                    )
                    urn.urn = redirected_urn
                    urn.save()
                    updated += 1

                modified += 1

        self.stdout.write(
            self.style.SUCCESS(
                (
                    f"\n\n{modified} wikidata URNs modified: "
                    f"{deleted} deleted, {updated} updated."
                )
            )
        )
