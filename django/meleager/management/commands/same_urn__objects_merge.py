import csv  # pas sûr que cela soit nécessaire
import io
import urllib.request

from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, Keyword, Language, KeywordCategory, Name, Passage, City
from meleager_user.models import User
from meleager.helpers.wikidata import get_wiki_names_from_url


class Command(BaseCommand):
    help = "Merge keywords and cities with same urn"




    def handle(self, *args, **options):
        urns_keywords_list=[]
        urns_cities_list=[]
        # urns_authors_list=[]
        urns = URN.objects.all()
        for urn in urns:
            if len(urn.keyword_set.all()) > 1:
                urns_keywords_list.append(urn.urn)
                
            if len(urn.city_set.all()) > 1:
                urns_cities_list.append(urn.urn)
        print(urns_keywords_list)
        print(urns_cities_list)
        for urn in urns_keywords_list:
            print('Treating: ')
            print(urn)
            keywords_to_merge = Keyword.objects.filter(alternative_urns__urn=urn)
            print(keywords_to_merge)
            for keyword in keywords_to_merge:
                print(keyword)
                print(keyword.category)
            keyword = keywords_to_merge.first()
            print('lala')
            print(keyword)
            for k in keywords_to_merge[1:]:
                print('lulu')
                print(k)
                for passage in k.passages.all():
                    keyword.passages.add(passage)
                for name in k.names.all():
                    keyword.names.add(name)
                k.delete()    
        for urn in urns_cities_list:
            cities_to_merge = City.objects.filter(alternative_urns__urn=urn)
            city = cities_to_merge.first()
            for k in cities_to_merge[1:]:
                for passage in k.passages.all():
                    city.passages.add(passage)
                for name in k.names.all():
                    city.names.add(name)
                k.delete()    
