from django.core.management.base import BaseCommand
from django.db.models import Q
from meleager.models import Alignment, Text


def check_texts():
    texts_associated_twice = Text.objects.exclude(passage=None).exclude(scholium=None)
    if not texts_associated_twice.count():
        return True

    print("The following texts are linked to both a Passage and a Scholium:")
    for text in texts_associated_twice:
        print(text)

    return False


def check_alignments():
    text_1_or_2_empty = Q(text_1=None) | Q(text_2=None)
    incorrect_alignments = Alignment.objects.filter(text_1_or_2_empty)
    if not incorrect_alignments:
        return True

    for alignment in incorrect_alignments:
        print(f"{alignment.pk} - {alignment.text_1} {alignment.text_2}")

    return False


class Command(BaseCommand):
    help = "Checks database integrity"

    def handle(self, *args, **options):
        texts_ok = check_texts()
        alignments_ok = check_alignments()

        if texts_ok and alignments_ok:
            print("All good.")
