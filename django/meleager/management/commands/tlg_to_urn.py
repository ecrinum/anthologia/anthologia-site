from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, Author




class Command(BaseCommand):
    help = "Convert tlg_id into an alternative urn"


    def handle(self, *args, **options):
        authors= Author.objects.filter(tlg_id__isnull=False)
        for author in authors:
            tlg= author.tlg_id.replace('tlg-','')
            tlgurn = 'http://data.perseus.org/catalog/urn:cts:greekLit:tlg'+tlg
            urn = URN.objects.create(source='tlg', urn = tlgurn)                                                                
            urn.author_set.add(author)
