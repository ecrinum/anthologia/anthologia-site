import codecs
import csv
from contextlib import closing

import requests
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Load keywords’ categories from distant CSV."

    def add_arguments(self, parser):
        parser.add_argument("--url")

    def handle(self, *args, **options):
        from meleager.models import KeywordCategory, Language, Name

        csv_url = options["url"]

        # See https://stackoverflow.com/a/38677650
        with closing(requests.get(csv_url, stream=True)) as response:
            csv_content = codecs.iterdecode(response.iter_lines(), "utf-8")
            categories_reader = csv.DictReader(csv_content, delimiter=";")
            languages = {
                lang: Language.objects.get(code=lang)
                for lang in categories_reader.fieldnames
            }

            # Fix the issue with `Lieux imaginaires` having the
            # `undefined` language in data.
            lieux_imaginaires = Name.objects.get(name="Lieux imaginaires")
            lieux_imaginaires.language = languages["fra"]
            lieux_imaginaires.save()

            for csv_category in categories_reader:
                # The French category is the one created by default.
                try:
                    category = KeywordCategory.objects.get(
                        names__name=csv_category["fra"].strip(),
                        names__language=languages["fra"],
                    )
                except KeywordCategory.DoesNotExist:
                    # It happens with the newly introduced `Mots-clés`.
                    keyword_name, created = Name.objects.get_or_create(
                        name=csv_category["fra"].strip(), language=languages["fra"]
                    )
                    category = KeywordCategory.objects.create()
                    category.names.add(keyword_name)

                for lang, category_name in csv_category.items():
                    # We consider French entries to be OK at this point.
                    if lang == "fra":
                        continue
                    # Create the name for the language and add it to the category.
                    try:
                        name, created = Name.objects.get_or_create(
                            name=category_name.strip(), language=languages[lang]
                        )
                    except Name.MultipleObjectsReturned:
                        # If it happens, it means that we have inconsistencies in data!
                        name = Name.objects.filter(
                            name=category_name.strip(), language=languages[lang]
                        ).first()
                        print(
                            (
                                f"A Name with name `{category_name.strip()}` "
                                f"and language `{lang}` has a duplicate."
                            )
                        )
                    category.names.add(name)
