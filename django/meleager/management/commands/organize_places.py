import csv
import io
import urllib.request

from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize
from meleager.models import URN, Keyword, City, KeywordCategory,Name,Language

from meleager.helpers.wikidata import get_wiki_names_from_url

def keywordsdicts():

    url = 'https://gitlab.huma-num.fr/ecrinum/anthologia/dataap/-/raw/master/data/keywords/peoplePLace.csv'
    webpage = urllib.request.urlopen(url)
    datareader = csv.DictReader(io.TextIOWrapper(webpage))
    return datareader

def changenames(my_object, object_wikidata):
# fonction pour supprimer les noms d'un mot-clé ou d'une ville et les remplacer avec les noms wikidata
    print('deleting names') 
    for name in my_object.names.all():
        name.delete()
    object_wiki_names=get_wiki_names_from_url(object_wikidata)    
    print('Object: ')
    print(object_wiki_names)
    for lang, object_name in object_wiki_names.items():
        print(lang)
        language = Language.objects.get(code=str(lang))
        try:
            name, created = Name.objects.get_or_create(name=object_name, language=language)
        except Name.MultipleObjectsReturned:
# dans le cas où il y ait plusieurs noms avec le champ nom égal, on prend le premier.
            name = Name.objects.filter(name=object_name).first()
        print('nom de l\'objet')
        print(name)
        my_object.names.add(name)

class Command(BaseCommand):
    help = "Organize keywords in the category Peuples, lieux"

    def handle(self, *args, **options):
        errors_list=[]

        for keyword in keywordsdicts():
            print('\n# Name: '+keyword['names'])
            print('id: '+keyword['id'])
            print('\n pleiades: '+keyword['pleiades'])
            print('\n transfert: '+keyword['transfert'])
            keyword_wikidata = keyword['wikidata'].strip()
            if keyword_wikidata not in ['','pas de wiki']:
                my_wikidata, created = URN.objects.get_or_create(urn=keyword_wikidata, defaults={'source':'wikidata'})

                print(my_wikidata)
                try:
                    my_keyword = Keyword.objects.get(unique_id=keyword["id"])
                    #print('nom dans la base:'+my_keyword.names.first().name)
                except Keyword.DoesNotExist:
                    errors_list.append(keyword)
                    print('ERROR!!! '+keyword['names']+ ' DOES NOT EXIST!!!!\n\n')
                    continue
                except Keyword.AttributeError:
                    errors_list.append(keyword)
                    print('ERROR!!! '+keyword['names']+ ' DOES NOT EXIST!!!!\n\n')
                    continue
                if keyword['transfert']=='1':
                    print('déplacer en cities')
                    my_city, created = City.objects.get_or_create(alternative_urns__urn=my_wikidata.urn)
                    # je ne sais pas pourquoi mais la city reste sans urn donc:
                    my_city.alternative_urns.add(my_wikidata)  
                    print('le wikiid suivant:')
                    print(my_wikidata.urn) 
                    print('Il s\'agit de la ville: ')
                    print(my_city.pk)
                    if keyword['pleiades'] != '':
                        print('Elle a un id pleiades!')
                        my_pleiades, created = URN.objects.get_or_create(urn=keyword['pleiades'], defaults={'source':'Pleiades'})
                        my_city.alternative_urns.add(my_pleiades)
                    print('on ajoute les noms à partir du wikiid suivant:')
                    print(my_wikidata.urn) 
                    print(get_wiki_names_from_url(my_wikidata.urn))
                    changenames(my_city,my_wikidata.urn)
                   # on associe les mêmes passages
                    print('maintenant on associe les passages')
                    for passage in my_keyword.passages.all():
                        print(passage)
                        my_city.passages.add(passage)
                    my_keyword.delete()    
                elif keyword['transfert']=='2':
                    print('reste en Peuples')
                    my_keyword.alternative_urns.add(my_wikidata)
                    changenames(my_keyword, my_wikidata.urn)
                elif keyword['transfert']=='3':
                    print('Déplacer en Lieux imaginaires')
                    category, created= KeywordCategory.objects.get_or_create(names__name='Lieux imaginaires')
                    # il n'ajoute pas le nom donc il faut le faire:
                    if created:
                        category_name=Name.objects.create(name='Lieux imaginaires')

                        category.names.add(category_name)
                        
                    print(category)
                    my_keyword.category=category
                    my_keyword.save()
                    my_keyword.alternative_urns.add(my_wikidata)
                    changenames(my_keyword, my_wikidata.urn)
            else:
                print('pas de wiki')
                
                if keyword['transfert']=='1':
                    print('déplacer en cities')
                    my_keyword = Keyword.objects.get(unique_id=keyword["id"])
                    keyword_names = my_keyword.names.all()
                    my_city = City.objects.create()
                    for name in keyword_names:
                        my_city.names.add(name)
                    for passage in my_keyword.passages.all():
                        print(passage)
                        my_city.passages.add(passage)
                    my_keyword.delete()    
                        
        print(errors_list)

