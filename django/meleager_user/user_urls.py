from django.urls import path

from .feed import LatestUsersFeed
from .views import ProfileView, UserHistoryView

urlpatterns = [
    path("latestusersfeed/", LatestUsersFeed(), name="latestusersfeed"),
    path("profile/", ProfileView.as_view(), name="profile"),
    path("<int:pk>/history/", UserHistoryView.as_view(), name="user-history"),
]
