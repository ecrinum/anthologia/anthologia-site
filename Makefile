integration_tests:
	uv pip install -r integration/requirements.txt
	uv run python -m playwright install
	cd django/ && DJANGO_SETTINGS_MODULE=anthology.settings.local uv run pytest ../integration --browser firefox --browser chromium --browser webkit --base-url http://127.0.0.1:8000

tests_ci:
	cd django/ && DJANGO_SETTINGS_MODULE=anthology.settings.gitlab_test uv run pytest

tests:
	cd django/ && DJANGO_SETTINGS_MODULE=anthology.settings.local uv run pytest

run:
	uv run django/manage.py runserver --settings=anthology.settings.local
